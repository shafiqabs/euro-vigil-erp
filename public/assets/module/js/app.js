$(document).ready(function() {

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)')
            .exec(window.location.search);
        return (results !== null) ? results[1] || 0 : false;
    }

    $(".number , .amount, .numeric").inputFilter(function(value) {
        return /^-?\d*[.,]?\d*$/.test(value); });

    $('.editor').summernote();

    $('#dataTable').DataTable( {
        "paging":   false,
         order: [[0, "asc"]],
    });

    $('#datatablePaging').DataTable( {
        "pageLength": 25,
        order: [[0, "DESC"]]
    });

    $('#datatableScroll').DataTable( {
        "scrollY": 450,
        "paging":   false,
        "scrollX": false
    });

    $('.select2-multiple').select2();
    $('.select2').select2();
    $('.mobileLocal').mask("00000-000000", {placeholder: "_____-______"});
    $('.phoneLocal').mask("0-00000000", {placeholder: "_____-______"});

    $('.form-submit').submit(function(){
        $("button[type='submit']", this)
            .html("Please Wait...")
            .attr('disabled', 'disabled');
        return true;
    });

    $(document).on('change', ".custom-file-input", function(e) {
        var id = $(this).attr('id');
        var fileName = document.getElementById(id).files[0].name;
        for (var i = 0; i < $("#"+id).get(0).files.length; ++i) {
            var file1=$("#"+id).get(0).files[i].name;
            if(file1){
                var file_size=$("#"+id).get(0).files[i].size;
                if(file_size < 2097152){
                    var ext = file1.split('.').pop().toLowerCase();
                    if($.inArray(ext,['jpg','jpeg','gif','png','pdf','xls','xlsx'])===-1){
                        alert("Invalid file extension");
                        return false;
                    }
                }else{
                    alert("Screenshot size is too large.");
                    return false;
                }
            }else{
                alert("fill all fields..");
                return false;
            }
        }
        var nextSibling = e.target.nextElementSibling
        nextSibling.innerText = fileName
    })

    $(".maxCheck").keydown(function () {
        // Save old value.
        var max = parseInt($(this).attr('max'));
        if (!$(this).val() || (parseInt($(this).val()) <= parseInt($(this).attr('max')) && parseInt($(this).val()) >= 0))
            $(this).data("old", $(this).val());
    }).keyup(function () {
        // Check correct, else revert back to old value.
        if (!$(this).val() || (parseInt($(this).val()) <= parseInt($(this).attr('max')) && parseInt($(this).val()) >= 0))
            ;
        else
            $(this).val($(this).data("old"));
    });

   /* $('body').on('keydown', 'input, select', function(e) {
        if (e.which === 13) {
            var self = $(this), form = self.parents('form:eq(0)'), focusable, next;
            focusable = form.find('input').filter(':visible');
            next = focusable.eq(focusable.index(this)+1);
            if (next.length) {
                next.focus();
            }
            return false;
        }
    });*/

   /* $("input").not($(":button")).keypress(function (evt) {
        if (evt.keyCode === 13) {
            iname = $(this).val();
           // if (iname !== 'Submit') {
                var fields = $(this).parents('form:eq(0),body').find('button, input, textarea, select');
                var index = fields.index(this);
                if (index > -1 && (index + 1) < fields.length) {
                    fields.eq(index + 1).focus();
                }
                return false;
           // }
        }
    });*/

    $("#startMonth, #endMonth , #startDate, #endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        yearRange: "-90:+00",
        showOn: "both",
        showButtonPanel: true,
        buttonImage: "/assets/images/icon-calendar-green.png",
        buttonImageOnly: true
        /* onClose: function(dateText, inst) {
             $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
         }*/
    });


    $('.tntDateaa').datepicker({
        format: "dd-mm-yyyy",
        todayHighlight:true,
        startDate: new Date(),
        autoclose: true
    }).datepicker('setDate', new Date());

    $('.datePicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        yearRange: "-10:+10",
        showOn: "both",
        showButtonPanel: true,
        buttonImage: "/assets/images/icon-calendar-green.png",
        buttonImageOnly: true
    });

     $('.dobDatePicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        yearRange: "-100:00",
        showOn: "both",
        showButtonPanel: true,
        buttonImage: "/assets/images/icon-calendar-green.png",
        buttonImageOnly: true
    });

    $('.symfonyPicker').datepicker({
        dateFormat: 'yyyy-MM-dd'
    });

    $( ".monthCalendar" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm-yy",
        yearRange: "-50:+00",
        showOn: "both",
        showButtonPanel: true,
        buttonImage: "/assets/images/icon-calendar-green.png",
        buttonImageOnly: true,
        onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });

    $('.timePicker').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: 'hh:mm tt'
    });


    $('.checkboxToggle').bootstrapToggle();

    $('.multi-select2').multiSelect({ selectableOptgroup: true });

    $('#optgroup').multiSelect({ selectableOptgroup: true });

    $(document).on('keypress', '.form-control', function(e) {
        value = $(this).val();
        $(e.target).closest('.form-group').find('.control-label').removeClass('required');
    });

    $(document).on('click', '.amount , .number-input', function() {
        val = $(this).val();
        if(val === 0 || val === '' ){
           // $(this).val("");
        }
    });

    var explode = function AutoReload()
    {
        $('#entityDatatable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    }

    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }


    $(document).on('click', '.entity-show', function(){

        $('#modalTitleLabel').html($(this).attr('data-title'));
        $.get($(this).attr('data-action'))
            .done(function(response) {
                $('#entity-data').html(response);
                $('#bootstrapModal').modal('show');
            });
    });

    $(".number , .amount").inputFilter(function(value) {
        return /^-?\d*[.,]?\d*$/.test(value); });

    $('.status').change(function() {
        console.log('Toggle: ' + $(this).prop('checked'))
    });


    $('.setupStatus').change(function() {
        console.log('Toggle: ' + $(this).prop('checked'))
    });

    $(document).on('change', '.status', function(){
        var status = $(this).prop('checked');
        $.get($(this).attr('data-action'),{'status':status})
            .done(function(response) {
                $('#entity-data').html(response)
            });
    })

    $(document).on('change', '.itemStatus', function(){
        var status = $(this).prop('checked');
        $.get($(this).attr('data-action'),{'status':status})
            .done(function(response) {
                $('#entity-data').html(response)
            });
    })

    $(document).on('change', '.transaction-method', function() {

        var transactionMethod = $(this).val();
        if(transactionMethod === "bank" ){
            $('.mobile-hide').hide(500).removeClass('show-method');
            $('.bank-hide').slideDown("slow");
        }else if(transactionMethod === 'mobile' ){
            $('.bank-hide').hide(500).removeClass('show-method');
            $('.mobile-hide').slideDown("slow");
        }else{
            $('.bank-hide,.mobile-hide').hide(500).removeClass('show-method');
        }

    });

    $(document).on("click", ".remove", function(event) {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $('#confirm-content').confirmModal({
            topOffset: 0,
            top: '25%',
            onOkBut: function(el) {
                $.get(url, function( data ) {
                    if(data === 'invalid'){
                        location.reload();
                    }else{
                        $(event.target).closest('tr').remove();
                    }
                });
            }
        });
    });

    $(document).on('click',".removeReload", function (event) {

        var url = $(this).attr('data-action');
        $('#confirm-content').confirmModal({
            topOffset: 0,
            top: '25%',
            onOkBut: function(el) {
                $.get(url, function( data ) {
                    location.reload();
                });
            }
        });

    });

    $('.employee-status').change(function() {
        let url = $(this).attr('data-action');
        let status =  $(this).prop('checked');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
            data: {status: status}
        })
    });

    $(document).on('click',".approveProcess", function (event) {

        var url = $(this).attr('data-action');
        $('#confirm-content').confirmModal({
            topOffset: 0,
            top: '25%',
            onOkBut: function(event, el) {
                $.get(url, function( data ) {
                    location.reload();
                });
            }
        });

    });

    $(document).on('click',".approve", function (event) {

        var url = $(this).attr('data-action');
        $('#confirm-content').confirmModal({
            topOffset: 0,
            top: '25%',
            onOkBut: function(event, el) {
                $.get(url, function( data ) {
                    location.reload();
                });
            }
        });

    });


    var explode = function AutoReload()
    {
        $('#entityDatatable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    }

    $("#allCheck").change(function(){  //"select all" change
        $(".process").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
        $('#check-btn').toggle();
    });

    $(document).on('click', '#checkBtn', function(e) {
        var favorite = [];
        $.each($("input[name='process']:checked"), function(){
            favorite.push($(this).val());
        });
        ids = favorite.join(", ");
        var url = $('#checkBtn').attr('data-action');
        $.get( url , { ids:ids } )
            .done(function( data ) {
                dt = $(this).dataTable();
                dt.fnDraw();
            });

    });

    var count = 0;
    var countId = 1;

    $('.addmore').click(function(){
        var $el = $(this);
        var $cloneBlock = $('#clone-block');
        var $clone = $cloneBlock.find('.clone:eq(0)').clone();
        //$clone.find('[id]').each(function(){this.id+=(++countId)});
        $clone.find('.form-control-file').attr('id', "addedxx"+(++countId));
        $clone.find(':text,textarea,file' ).val("");
        $clone.attr('id', "added"+(++count));
        $clone.find('.row-remove').removeClass('hidden');
        $cloneBlock.append($clone);

    });

    $('#clone-block').on('click', '.row-remove', function(){
        $(this).closest('.clone').remove();
    });


    $(document).on("click", ".meta-remove", function(event) {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $('#confirm-content').confirmModal({
            topOffset: 0,
            top: '25%',
            onOkBut: function(el) {
                $.get(url, function( data ) {
                    $(event.target).closest('.clone-remove').remove();
                });
            }
        });
    });

    $(document).on("click", ".attach-remove", function(event) {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $('#confirm-content').confirmModal({
            topOffset: 0,
            top: '25%',
            onOkBut: function(el) {
                $.get(url, function( data ) {
                    $(event.target).closest('.clone-remove').remove();
                   // $(this).closest('.clone-remove').remove();
                    //$('remove-'+id).remove();
                });
            }
        });
    });


    // Sortable Code
    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width())
        });
        return $helper;
    };

    $(document).on('change', '#autocomplete', function() {
        var employee = $(this).val();
        alert(employee);
    });

    $(".table-sortable tbody").sortable({
        helper: fixHelperModified
    }).disableSelection();

    $(".table-sortable thead").disableSelection();

    $(document).on('opened', '.remodal', function () {

        var id = $.urlParam('process');
        var check = $.urlParam('check');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url, function(){
            formCommonProcess();
        });
    });

    function formCommonProcess() {

    }

});
