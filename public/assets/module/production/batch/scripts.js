$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}
pathUrl = window.location.href;
var pathname = window.location.pathname; // Returns path only (/path/example.html)
var segments = pathUrl.split( '/' );
var lang = segments[3];

$( document ).ready(function( $ ) {

    $(document).on('click',".addVendor", function (event) {
        var url = $(this).attr('data-action');
        $.MessageBox({
            message : "<b>ADD VENDOR</b>",
            buttonDone      : {
                save : {
                    text        : "Save",
                    customClass : "custom_button",
                    keyCode     : 13
                }
            },
            buttonFail      : "Cancel",
            input   : {
                vendorType : {
                    type         : "select",
                    label        : "Vendor Type",
                    title        : "Select a vendor type",
                    options      : ["Registered","Non-registered","Turnover","Foreign"]
                },
                businessType : {
                    type         : "select",
                    label        : "Vendor Business Type",
                    title        : "Select a vendor business type",
                    options      : ["General", "Group of Companies","Limited","Properties","Others"]
                },
                company    : {
                    type         : "text",
                    label        : "Vendor Company",
                    title        : "Enter Vendor company name"
                },
                name    : {
                    type         : "text",
                    label        : "Vendor Name",
                    title        : "Enter Vendor Name"
                },
                mobile    : {
                    type         : "text",
                    label        : "Vendor Mobile",
                    title        : "Enter Vendor mobile no"
                },
                binNo    : {
                    type         : "text",
                    label        : "Vendor BIN No",
                    title        : "Enter vendor BIN no"
                },
                nidNo    : {
                    type         : "text",
                    label        : "Vendor NID No",
                    title        : "Enter vendor NID no"
                },
                address    : {
                    type         : "text",
                    label        : "Vendor Address",
                    title        : "Enter vendor address"
                }

            },
            filterDone      : function(data){
                if (data['vendorType'] === null) return "The vendor type field is required";
                if (data['name'] === "") return "The vendor name field is required";
                if (data['company'] === "") return "The vendor company name field is required";
                if (data['mobile'] === "") return "The vendor mobile field is required";
                return $.ajax({
                    url     : url,
                    type    : "post",
                    data    : data
                }).then(function(response){
                    if (response == false) return "Wrong username or password";
                });
            },
            top     : "auto"
        }).done(function(data){
            if(data === 'invalid'){
                $.MessageBox("This vendor already exist, Please try another company name");
            }else{
                location.reload();
            }
        });

    });

    $(document).on('change', '.vendor', function() {
        vendor = $(this).val();
        $.get(  "/"+lang+"/production/batch/vendor-info", { vendor: vendor } )
            .done(function( response ) {
                obj = JSON.parse(response);
                $('#binNo').html(obj['binNo']);
                $('#vendorMobile').html(obj['vendorMobile']);
                $('#vendorAddress').html(obj['vendorAddress']);
            });


    });

    $(document).on('click', '#addProduct', function() {

        var productId = $('#product').val();
        var issueQuantity = $('#issueQuantity').val();
        var receiveQuantity = ($('#receiveQuantity').val()) ? parseFloat($('#receiveQuantity').val()) : 0;
        var damageQuantity = ($('#damageQuantity').val()) ? parseFloat($('#damageQuantity').val()) : 0;
        var url = $('#addProduct').attr('data-action');
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        if(issueQuantity === ''){
            $.MessageBox("Please enter issue quantity");
            $('#quantity').focus();
            return false;
        }
        var receive = (receiveQuantity + damageQuantity);
        if(financial(issueQuantity) !== financial(receive)){
            $.MessageBox("Issue quantity & Receive quantity does not match");
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&issueQuantity='+issueQuantity+'&receiveQuantity='+receiveQuantity+'&damageQuantity='+damageQuantity,
            success: function (response) {
                location.reload();
            }
        })
    });

    $(document).on('click', '.updateProduct', function() {

        var id = $(this).attr('data-id');
        var issueQuantity = $('#issueQuantity-'+id).val();
        var receiveQuantity = ($('#receiveQuantity-'+id).val()) ? parseFloat($('#receiveQuantity-'+id).val()) : 0;
        var damageQuantity = ($('#damageQuantity-'+id).val()) ? parseFloat($('#damageQuantity-'+id).val()) : 0;
        var url = $(this).attr('data-action');

        if(issueQuantity === ''){
            $.MessageBox("Please enter issue quantity");
            $('#quantity').focus();
            return false;
        }
        if(receiveQuantity === ''){
            $.MessageBox("Please enter receive quantity");
            $('#quantity').focus();
            return false;
        }
        var receive = (receiveQuantity + damageQuantity);
        if(financial(issueQuantity) !== financial(receive)){
            $.MessageBox("Issue quantity & Receive quantity does not match");
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'issueQuantity='+issueQuantity+'&receiveQuantity='+receiveQuantity+'&damageQuantity='+damageQuantity,
            success: function (response) {
                location.reload();
            }
        })
    });

    $(document).on('click', '#addContractualProduct', function() {

        var url = $(this).attr('data-action');
        var productId = $('#product').val();
        var issueQuantity = $('#issueQuantity').val();
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        if(issueQuantity === ''){
            $.MessageBox("Please enter issue quantity");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&issueQuantity='+issueQuantity+'&receiveQuantity=0&damageQuantity=0',
            success: function (response) {
                location.reload();
            }
        })
    });

    $(document).on('click', '.updateContractualProduct', function() {

        var id = $(this).attr('data-id');
        var issueQuantity = $('#issueQuantity-'+id).val();
        var url = $(this).attr('data-action');
        if(issueQuantity === ''){
            $.MessageBox("Please enter issue quantity");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'issueQuantity='+issueQuantity+'&receiveQuantity=0&damageQuantity=0',
            success: function (response) {
                location.reload();
            }
        })
    });


    $(document).on('click',".receiveContractualProduct", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var receiveQuantity = ($('#receiveQuantity-'+id).val()) ? parseFloat($('#receiveQuantity-'+id).val()) : 0;
        var damageQuantity = ($('#damageQuantity-'+id).val()) ? parseFloat($('#damageQuantity-'+id).val()) : 0;
        var returnQuantity = ($('#returnQuantity-'+id).val()) ? parseFloat($('#returnQuantity-'+id).val()) : 0;
        var remainingQuantity = ($('#remainingQuantity-'+id).val()) ? parseFloat($('#remainingQuantity-'+id).val()) : 0;
        var totalQnt = receiveQuantity + damageQuantity +  returnQuantity;
        if(totalQnt > remainingQuantity){
            $.MessageBox("Receive quantity must be need equal or less then remaining quantity");
            return false;
        }
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to delete this record?"
        }).done(function(){
            $.ajax({
                url: url,
                type: 'POST',
                data: 'returnQuantity='+returnQuantity+'&receiveQuantity='+receiveQuantity+'&damageQuantity='+damageQuantity,
                success: function (response) {
                     location.reload();
                }
            });
        });

    });

    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $(document).on('change', '.action', function () {
        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false
        });
    });


});




