$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}
pathUrl = window.location.href;
var pathname = window.location.pathname; // Returns path only (/path/example.html)
var segments = pathUrl.split( '/' );
var lang = segments[3];

$( document ).ready(function( $ ) {

    $(document).on('change', '.action', function () {
        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false
        });
    });


});




