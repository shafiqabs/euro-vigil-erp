$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}
var pathname = window.location.pathname; // Returns path only (/path/example.html)
var url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
var origin   = window.location.origin;   // Returns base URL (https://example.com)


$( document ).ready(function( $ ) {


    $(document).on('opened', '.remodal', function () {
        var id = $.urlParam('process');
        var check = $.urlParam('check');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url, function(){
            formCommonProcess();
            if(check === 'edit'){
                formEditSubmitProcess();
            }else{
                formSubmitProcess();
            }
        });
    });


    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.mobileLocal').mask("00000-000000", {placeholder: "_____-______"});

    $('.checkboxToggle').bootstrapToggle();

    $('.multi-select2').multiSelect({ selectableOptgroup: true });

    $('#optgroup').multiSelect({ selectableOptgroup: true });

    $('.select2').select2({
        theme: 'bootstrap4'
    });

 /*   $('.hover').tooltip({
        title: fetchData,
        html: true,
        placement: 'right'
    });*/
});

function formCommonProcess() {


}

var explode = function AutoReload()
{
    $('#entityDatatable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    });
    $('.form-submit').html("Save & Continue").prop("disabled", false);
}

function formSubmitProcess() {



    $("#postForm").validate({

        rules: {
            "terminal[mobile]": {
                required: true,
                remote:window.location.pathname+"creatable/available"
            },
             "terminal[organizationName]": {
                required: true,
                remote:window.location.pathname+"creatable/available"
            },
            "terminal[domain]": {
                required: false,
                remote:window.location.pathname+"creatable/available"
            },
            "terminal[subDomain]": {
                required: false,
                remote:window.location.pathname+"creatable/available"
            }
        },

        messages: {
            "terminal[mobile]":{
                required: "Please enter owner mobile no",
                remote: jQuery.validator.format("{0} mobile is already in use!")
            },
            "terminal[organizationName]":{
                required: "Please enter your organization name.",
                remote: jQuery.validator.format("{0} organization is already in use!")
            },
            "terminal[domain]":{
                remote: jQuery.validator.format("{0} domain is already in use!")
            },
            "terminal[subDomain]":{
                remote: jQuery.validator.format("{0} sub-domain is already in use!")
            }
        },
        submitHandler: function(form) {
            $(".form-submit").prop("disabled", true);
            $.ajax({
                url         : $('form#postForm').attr( 'action' ),
                type        : $('form#postForm').attr( 'method' ),
                data        : new FormData($('form#postForm')[0]),
                processData : false,
                contentType : false,
                beforeSend: function() {
                    $('.form-submit').html("Loading...").attr('disabled', 'disabled');
                },
                success: function(response){
                    $('form#postForm')[0].reset();
                    $("#process-msg").show();
                    $(".alert-success").html(response);
                    setTimeout( explode, 2000);
                }
            });
        }
    });
}

function formEditSubmitProcess() {

    $("#postForm").validate({

        rules: {
            rules: {
                "terminal[mobile]": {
                    required: true,
                    remote:window.location.pathname+"editable/available"
                },
                "terminal[organizationName]": {
                    required: true,
                    remote:window.location.pathname+"editable/available"
                },
                "terminal[domain]": {
                    required: false,
                    remote:window.location.pathname+"editable/available"
                },
                "terminal[subDomain]": {
                    required: false,
                    remote:window.location.pathname+"editable/available"
                }
            },
        },

        messages: {
            "terminal[mobile]":{
                required: "Please enter owner mobile no",
                remote: jQuery.validator.format("{0} mobile is already in use!")
            },
            "terminal[organizationName]":{
                required: "Please enter your organization name.",
                remote: jQuery.validator.format("{0} organization is already in use!")
            },
            "terminal[domain]":{
                remote: jQuery.validator.format("{0} domain is already in use!")
            },
            "terminal[subDomain]":{
                remote: jQuery.validator.format("{0} sub-domain is already in use!")
            }
        },
        submitHandler: function(form) {

            $(".form-submit").prop("disabled", true);
            $.ajax({
                url         : $('form#postForm').attr( 'action' ),
                type        : $('form#postForm').attr( 'method' ),
                data        : new FormData($('form#postForm')[0]),
                processData : false,
                contentType : false,
                beforeSend: function() {
                    $('.form-submit').html("Loading...").attr('disabled', 'disabled');
                },
                success: function(response){
                    $("#process-msg").show();
                    $(".alert-success").html(response);
                    setTimeout( explode, 2000);
                }
            });
        }
    });
}

function fetchData()
{
    var fetch_data = '';
    var element = $(this);
    var id = element.attr("id");
    $.ajax({
        url:"fetch.php",
        method:"POST",
        async: false,
        data:{id:id},
        success:function(data)
        {fetch_data = data;}
    });
    return fetch_data;
}

