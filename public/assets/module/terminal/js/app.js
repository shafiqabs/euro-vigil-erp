$(document).ready(function( $ ) {

    $('.select2-multiple').select2();
    $('.select2').select2();

    $('.form-submit').submit(function(){
        $("button[type='submit']", this)
            .html("Please Wait...")
            .attr('disabled', 'disabled');
        return true;
    });

    $(document).on('keypress', '.form-control', function(e) {
        value = $(this).val();
        $(e.target).closest('.form-group').find('.control-label').removeClass('required');
    });

});
