
$( document ).ready(function( $ ) {

    currentUrl = window.location.href;
    var segments = currentUrl.split( '/' );
    var lang = segments[3];

    $('.datePicker').datepicker({
        dateFormat: 'dd-mm-yy'
    });

    $('.timePicker').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: 'hh:mm tt'
    });

    $(document).on('click',".addVendor", function (event) {

        var url = $(this).attr('data-action');
        $.MessageBox({
            message : "<b>ADD VENDOR</b>",
            buttonDone      : {
                save : {
                    text        : "Save",
                    customClass : "custom_button",
                    keyCode     : 13
                }
            },
            buttonFail      : "Cancel",
            input   : {
                vendorType : {
                    type         : "select",
                    label        : "Vendor Type",
                    title        : "Select a vendor type",
                    options      : ["Registered","Non-registered","Turnover","Foreign"]
                },
                businessType : {
                    type         : "select",
                    label        : "Vendor Business Type",
                    title        : "Select a vendor business type",
                    options      : ["General", "Group of Companies","Limited","Properties","Others"]
                },
                company    : {
                    type         : "text",
                    label        : "Vendor Company",
                    title        : "Enter Vendor company name"
                },
                name    : {
                    type         : "text",
                    label        : "Vendor Name",
                    title        : "Enter Vendor Name"
                },
                mobile    : {
                    type         : "text",
                    label        : "Vendor Mobile",
                    title        : "Enter Vendor mobile no"
                },
                binNo    : {
                    type         : "text",
                    label        : "Vendor BIN No",
                    title        : "Enter vendor BIN no"
                },
                nidNo    : {
                    type         : "text",
                    label        : "Vendor NID No",
                    title        : "Enter vendor NID no"
                },
                address    : {
                    type         : "text",
                    label        : "Vendor Address",
                    title        : "Enter vendor address"
                }

            },
            filterDone      : function(data){
                if (data['vendorType'] === null) return "The vendor type field is required";
                if (data['name'] === "") return "The vendor name field is required";
                if (data['company'] === "") return "The vendor company name field is required";
                if (data['mobile'] === "") return "The vendor mobile field is required";
                return $.ajax({
                    url     : url,
                    type    : "post",
                    data    : data
                }).then(function(response){
                    if (response == false) return "Wrong username or password";
                });
            },
            top     : "auto"
        }).done(function(data){
            if(data === 'invalid'){
                $.MessageBox("This vendor already exist, Please try another company name");
            }else{
                location.reload();
            }
        });

    });


    $(document).on('change', '.vendor', function() {
        vendor = $(this).val();
        $.get(  "/"+lang+"/inventory/purchase/vendor-info", { vendor: vendor } )
            .done(function( response ) {
                obj = JSON.parse(response);
                $('#binNo').html(obj['binNo']);
                $('#vendorMobile').html(obj['vendorMobile']);
                $('#vendorAddress').html(obj['vendorAddress']);
            });


    });

    $(document).on('change', '#product', function() {

        var url = $('#product').val();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                obj = JSON.parse(response);
                $('#productId').val(obj['productId']);
                $('#purchasePrice').val(obj['purchasePrice']);
                $('#salesPrice').val(obj['salesPrice']);
                $('#unit').html(obj['unit']);
              /*  $('.vatProcess').html(obj['vatMode']);*/
            }
        })
    });

    $(document).on('change', '#purchaseInputTax', function() {

        var vatProcess = $(this).val();
        var url = $(this).attr('data-action');
        $.get( url, { particular:vatProcess} )
            .done(function( data ) {
                $('.vatProcess').html(data);
            });


    });

    $(document).on('click', '#addLocalProduct', function() {

        vdsApplicable = 0;
        rebateApplicable = 0;
        var productId = $('#productId').val();
        var purchaseInputTax =  $('#purchaseInputTax').val();
        var quantity = $('#quantity').val();
        var purchasePrice = $('#purchasePrice').val();
        var vatPercent =  Number.parseFloat($('#vatPercent').val());
        var supplementaryDuty = $('#supplementaryDuty').val();
        if ($("#vdsApplicable:checked").length > 0 ) {
            var vdsApplicable = 1;
        }
        if ($("#rebateApplicable:checked").length > 0 ) {
            var rebateApplicable = 1;
        }

        var salesPrice = 0;
        var url = $(this).attr('data-action');
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        if(purchaseInputTax === ''){
            $.MessageBox("Please select purchase input tax");
            $('#purchaseInputTax').focus();
            return false;
        }
        if(purchasePrice === ''){
            $.MessageBox("Please enter purchase price");
            $('#purchasePrice').focus();
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Please enter purchase quantity");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&purchaseInputTax='+purchaseInputTax+'&quantity='+quantity+'&purchasePrice='+purchasePrice+'&salesPrice='+salesPrice+'&vatPercent='+vatPercent+'&supplementaryDuty='+supplementaryDuty+'&vdsApplicable='+vdsApplicable+'&rebateApplicable='+rebateApplicable,
            success: function (response) {
                setTimeout(jsonResult(response),100);
                obj = JSON.parse(response);
                $('#purchasePrice').val('');
                $("#product").select2('open');
                $('#price').val('');
                $('#supplementaryDuty').val('');
                $('#purchaseInputTax').prop('selectedIndex',0);
                $('#unit').html('Unit');
                $('#quantity').val('');

            }
        })
    });

    $(document).on('click', '#addProduct', function() {

        rebateApplicable = 0;
        var productId = $('#productId').val();
        var quantity = $('#quantity').val();
        var purchasePrice = $('#purchasePrice').val();
        var purchaseInputTax =  $('#purchaseInputTax').val();
        if ($("#rebateApplicable:checked").length > 0 ) {
            var rebateApplicable = 1;
        }
        var url = $('#addProduct').attr('data-action');
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        if(purchaseInputTax === ''){
            $.MessageBox("Please select purchase input tax");
            $('#purchaseInputTax').focus();
            return false;
        }
        if(purchasePrice === ''){
            $.MessageBox("Please enter purchase price");
            $('#purchasePrice').focus();
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Please enter purchase quantity");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&quantity='+quantity+'&purchasePrice='+purchasePrice+'&rebateApplicable='+rebateApplicable+'&purchaseInputTax='+purchaseInputTax,
            success: function (response) {
                obj = JSON.parse(response);
                setTimeout(jsonResult(response),100);
                $('#purchasePrice').val('');
                $("#product").select2().select2("val","").select2('open');
                $('#price').val('');
                $('#purchaseInputTax').prop('selectedIndex',0);
                $('#unit').html('Unit');
                $('#quantity').val('1');
            }
        })
    });

    $(document).on('change', '.quantity , .purchasePrice', function() {

        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var purchasePrice = parseFloat($('#purchasePrice-'+id).val());
        var salesQuantity = parseFloat($('#salesQuantity-'+id).val());
        if(salesQuantity > quantity){
            $('#quantity-'+id).val($('purchaseQuantity-'+id).val());
            $.MessageBox("Purchase quantity must be more then sales quantity.");
            return false;
        }
        $.ajax({
            "url":  "/"+lang+"/inventory/purchase/purchase-item-update",
            type: 'POST',
            data:'purchaseItem='+ id +'&quantity='+ quantity +'&purchasePrice='+ purchasePrice,
            success: function(response) {
                obj = JSON.parse(response);
                setTimeout(jsonResult(response),100);
            }

        })
    });

    $(document).on('click',".item-remove", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonDone  : "Yes",
            buttonFail  : "No",
            message     : "Are you sure you want to delete this record?"
        }).done(function(){
            $.get(url, function( response ) {
                obj = JSON.parse(response);
                $(event.target).closest('tr').hide();
                setTimeout(jsonResult(response),100);
            });
        });
    });

    function jsonResult(response) {

        obj = JSON.parse(response);
        $('#invoiceItems').html(obj['invoiceItems']);
        $('#invoiceQuantity').html(obj['quantity']);
        $('#invoiceSubTotal').html(obj['subTotal']);
        $('#invoiceCD').html(obj['cd']);
        $('#invoiceRD').html(obj['rd']);
        $('#invoiceSD').html(obj['sd']);
        $('#invoiceVAT').html(obj['vat']);
        $('#invoiceAIT').html(obj['ait']);
        $('#invoiceAT').html(obj['at']);
        $('#invoiceTTI').html(obj['tti']);
        $('#invoiceTotal').html(obj['total']);
        $('#discount').html(obj['discount']);
        $('#netTotal').html(obj['netTotal']);
        $('#paymentTotal').val(obj['netTotal']);
        $('#duable').html(obj['due']);
        if(obj['discountMode'] === "percent"){
            $('#discountPercent').html(obj['discountPercent']+'%');
        }
    }


    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    $('form#postForm').on('change', '.payment', function (e) {
        var mrp = $(this).val();
        var netTotal = Number.parseFloat($("#paymentTotal").val());
        $('#payable').html(financial(mrp));
        due = financial(netTotal - mrp);
        $('#duable').html(due);
    });



    $(document).on('change', '.purchase', function () {
        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false,
            success: function (response) {
                obj = JSON.parse(response);
                $('#subTotal').html(obj['subTotal']);
                $('#totalVat').html(obj['totalVat']);
                $('#total').html(obj['total']);
                $('#discount').html(obj['discount']);
                $('#netTotal').html(obj['netTotal']);
                $('#paymentTotal').val(obj['paymentTotal']);
                $('#duable').html(obj['due']);
                if(obj['discountMode'] === "percent"){
                    $('#discountPercent').html(obj['discountPercent']+'%');
                }
            }
        });

    });

    $(document).on('keypress', '.input', function (e) {

        if (e.keyCode === 13  ){
            var inputs = $(this).parents("form").eq(0).find("input,select");
            console.log(inputs);
            var idx = inputs.index(this);
            if (idx === inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {

                case 'product':
                    $('#purchaseInputTax').focus();
                    break;

                case 'purchaseInputTax':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    $('#addProduct').click();
                    $('#product').select2('open');
                    break;
            }
            return false;
        }
    });

    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });

    $(document).on('opened', '.remodal', function () {

        var id = $.urlParam('process');
        var check = $.urlParam('check');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url, function(){
            formCommonProcess();
            if(check === 'edit'){
                formEditSubmitProcess();
            }else{
                formSubmitProcess();
            }
        });
    });

});



