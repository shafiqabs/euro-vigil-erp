
$( document ).ready(function( $ ) {

    currentUrl = window.location.href;
    var segments = currentUrl.split( '/' );
    var lang = segments[3];

    $('.datePicker').datepicker({
        dateFormat: 'dd-mm-yy'
    });

    $('.timePicker').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: 'hh:mm tt'
    });

    $(document).on('click',".addCustomer", function (event) {
        var url = $(this).attr('data-action');
        $.MessageBox({
            message : "<b>ADD NEW CUSTOMER</b>",
            buttonDone      : {
                save : {
                    text        : "Save",
                    customClass : "custom_button",
                    keyCode     : 13
                }
            },
            buttonFail      : "Cancel",
            input   : {

                businessType : {
                    type         : "select",
                    label        : "Customer Business Type",
                    title        : "Select a customer business type",
                    options      : ["General", "Group of Companies","Limited","Properties","Others"]
                },
                company    : {
                    type         : "text",
                    label        : "Customer Company",
                    title        : "Enter Customer company name"
                },
                name    : {
                    type         : "text",
                    label        : "Customer Name",
                    title        : "Enter Customer Name"
                },
                mobile    : {
                    type         : "text",
                    label        : "Customer Mobile",
                    title        : "Enter Customer mobile no"
                },
                binNo    : {
                    type         : "text",
                    label        : "Customer BIN No",
                    title        : "Enter customer BIN no"
                },
                address    : {
                    type         : "text",
                    label        : "Customer Address",
                    title        : "Enter customer address"
                }

            },
            filterDone      : function(data){
                if (data['vendorType'] === null) return "Please fill the customer type";
                if (data['name'] === "") return "Please fill customer name";
                if (data['mobile'] === "") return "Please fill customer mobile no";
                return $.ajax({
                    url     : url,
                    type    : "post",
                    data    : data
                }).then(function(response){
                    if (response == false) return "Wrong username or password";
                });
            },
            top     : "auto"
        }).done(function(data){
            if(data === 'invalid'){
                $.MessageBox("This customer already exist, Please try another company name");
            }else{
                location.reload();
            }
        });

    });

    $(document).on('change', '.customer', function() {
        customer = $(this).val();
        $.get(  "/"+lang+"/inventory/sales/customer-info", { customer: customer } )
            .done(function( response ) {
                obj = JSON.parse(response);
                $('#binNo').html(obj['binNo']);
                $('#customerMobile').html(obj['customerMobile']);
                $('#customerAddress').html(obj['customerAddress']);
            });


    });

    $(document).on('change', '#product', function() {

        var url = $('#product').val();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                obj = JSON.parse(response);
                $('#productId').val(obj['productId']);
                $('#salesPrice').val(obj['salesPrice']);
                $('#price').html(obj['salesPrice']);
                $('#unit').html(obj['unit']);
                $('#remaining').html(obj['remainingQnt']);
                $('#remainingQnt').val(obj['remainingQnt']);
                $('#valueAddedTaxPercent').val(obj['valueAddedTaxPercent']);
                $('#batchItem').html(obj['batchItems']);
              //  $('.vatProcess').html(obj['vatProcess']);

            }
        })
    });

    $(document).on('change', '#purchaseInputTax', function() {

        var vatProcess = $(this).val();
        var url = $(this).attr('data-action');
        $.get( url, { particular:vatProcess} )
            .done(function( data ) {
                $('.vatProcess').html(data);
            });


    });

    $(document).on('change', '#productionBatch', function() {
       var url = $(this).attr('data-action');
       id =  $(this).val();
       if(id === ""){
           return false;
       }
       $.get( url,{item:id})
            .done(function(data) {
                $('#remaining').html(data);
                $('#batchQnt').val(data);
            });

    });

    $(document).on('click', '#addProduct', function() {

        vdsApplicable = 0;
        var productId = $('#productId').val();
        var purchaseInputTax = $('#purchaseInputTax').val();
        var productionBatch = $('.batchItem').val();
        var batchQnt = $('#batchQnt').val();
        var quantity = $('#quantity').val();
        var salesPrice = $('#salesPrice').val();
        var vatPercent = $('#vatPercent').val();
        if ($("#vdsApplicable:checked").length > 0 ) {
            var vdsApplicable = 1;
        }
        var url = $('#addProduct').attr('data-action');
        if(productId === ''){
            $('#product').prop('selectedIndex',0);
            return false;
        }
        if(purchaseInputTax === ''){
            $.MessageBox("Please select sales output tax");
            $('#purchaseInputTax').focus();
            return false;
        }
        if(productionBatch === ''){
            $.MessageBox("Item batch must be required");
            $('.batchItem').focus();
            return false;
        }
        if(salesPrice === ''){
            $.MessageBox("Price must be required");
            $('#salesPrice').focus();
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Quantity must be required");
            $('#quantity').focus();
            return false;
        }

        if(parseFloat(quantity) > parseFloat(batchQnt)){
            $('#quantity').focus();
            $.MessageBox("Sales quantity must be less/equal form the remaining quantity");
            return false;
        }

        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&quantity='+quantity+'&salesPrice='+salesPrice+'&productionBatch='+productionBatch+'&vdsApplicable='+vdsApplicable+'&vatPercent='+vatPercent+'&purchaseInputTax='+purchaseInputTax,
            success: function (response) {
                obj = JSON.parse(response);
                setTimeout(jsonResult(response),100);
                $('#product').prop('selectedIndex',0);
                $('#productionBatch').prop('selectedIndex',0);
                $('#salesPrice').val('');
                $('#unit').html('Unit');
                $('#quantity').val('');
            }
        })
    });

    $(document).on('change', '.quantity  ,.salesPrice', function() {

        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var salesPrice = parseFloat($('#salesPrice-'+id).val());
        var vatPercent = parseFloat($('#vatPercent-'+id).val());
        var subTotal  = (quantity * salesPrice);
        var vat  = ((subTotal * vatPercent)/100);
        $("#subTotal-"+id).html(subTotal);
        $("#vatTotal-"+id).html(vat);
        $("#total-"+id).html(subTotal + vat);
        $.ajax({
            "url":  "/"+lang+"/inventory/sales/sales-item-update",
            type: 'POST',
            data:'item='+ id +'&quantity='+ quantity+'&salesPrice='+ salesPrice,
            success: function(response) {
                setTimeout(jsonResult(response),100);
            }

        })
    });

    $(document).on('click',".item-remove", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonDone  : "Yes",
            buttonFail  : "No",
            message     : "Are you sure you want to delete this record?"
        }).done(function(){
            $.get(url, function( response ) {
                setTimeout(jsonResult(response),100);
                $(event.target).closest('tr').hide();

            });
        });
    });

    function jsonResult(response) {
        obj = JSON.parse(response);
        $('#invoiceItems').html(obj['invoiceItems']);
        $('#subTotal').html(obj['subTotal']);
        $('#totalSd').html(obj['totalSd']);
        $('#totalVat').html(obj['totalVat']);
        $('#totalTti').html(obj['totalTti']);
        $('#total').html(obj['total']);
        $('#discount').html(obj['discount']);
        $('#netTotal').html(obj['netTotal']);
        $('#paymentTotal').val(obj['netTotal']);
        $('#totalQuantity').html(obj['totalQuantity']);
        $('#totalItem').html(obj['totalItem']);
        $('#duable').html(obj['due']);
        if(obj['discountMode'] === "percent"){
            $('#discountPercent').html(obj['discountPercent']+'%');
        }
    }



    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    $('form#postForm').on('change', '.payment', function (e) {
        var mrp = $(this).val();
        var netTotal = Number.parseFloat($("#paymentTotal").val());
        $('#payable').html(financial(mrp));
        due = financial(netTotal - mrp);
        $('#duable').html(due);
    });



    $(document).on('change', '.action', function () {

        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false,
            success: function (response) {
                obj = JSON.parse(response);
                setTimeout(jsonResult(response),100);
            }
        });

    });

    $(document).on('keypress', '.input', function (e) {

        if (e.keyCode === 13  ){
            var inputs = $(this).parents("form").eq(0).find("input,select");
            console.log(inputs);
            var idx = inputs.index(this);
            if (idx === inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {

                case 'product':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    $('#addProduct').click();
                    $('#product').select2('open');
                    break;
            }
            return false;
        }
    });

    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });

    $(document).on('opened', '.remodal', function () {

        var id = $.urlParam('process');
        var check = $.urlParam('check');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url, function(){
            formCommonProcess();
            if(check === 'edit'){
                formEditSubmitProcess();
            }else{
                formSubmitProcess();
            }
        });
    });

});



