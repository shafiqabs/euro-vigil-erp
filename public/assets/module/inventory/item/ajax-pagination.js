$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

    //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": "/"+lang+"/inventory/item/data-table", // ajax source
            'data': function(data){

                var hsCode = $('#hsCode').val();
                var groupName = $('#groupName').val();
                var masterName = $('#masterName').val();
                var brand = $('#brand').val();
                var name = $('#name').val();
                var unitName = $('#unitName').val();
                var sku = $('#sku').val();
                var purchasePrice = $('#purchasePrice').val();
                var salesPrice = $('#salesPrice').val();
                var openingQuantity = $('#openingQuantity').val();
                var purchaseQuantity = $('#purchaseQuantity').val();
                var purchaseReturnQuantity = $('#purchaseReturnQuantity').val();
                var salesQuantity = $('#salesQuantity').val();
                var salesReturnQuantity = $('#salesReturnQuantity').val();
                var damageQuantity = $('#damageQuantity').val();
                var ongoingQuantity = $('#ongoingQuantity').val();
                var remainingQuantity = $('#remainingQuantity').val();

                // Append to data
                //  data._token = CSRF_TOKEN;

                data.hsCode = hsCode;
                data.groupName = groupName;
                data.masterName = masterName;
                data.brand = brand;
                data.name = name;
                data.unitName = unitName;
                data.sku = sku;
                data.purchasePrice = purchasePrice;
                data.salesPrice = salesPrice;
                data.openingQuantity = openingQuantity;
                data.purchaseQuantity = purchaseQuantity;
                data.purchaseReturnQuantity = purchaseReturnQuantity;
                data.salesQuantity = salesQuantity;
                data.salesReturnQuantity = salesReturnQuantity;
                data.damageQuantity = damageQuantity;
                data.ongoingQuantity = ongoingQuantity;
                data.remainingQuantity = remainingQuantity;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'hsCode' },
            { "name": 'groupName' },
            { "name": 'masterName' },
            { "name": 'brand' },
            { "name": 'name' },
            { "name": 'unitName','orderable':false  },
            { "name": 'sku' ,'orderable':false },
            { "name": 'purchasePrice','orderable':false  },
            { "name": 'salesPrice','orderable':false  },
            { "name": 'openingQuantity','orderable':false  },
            { "name": 'purchaseQuantity','orderable':false  },
            { "name": 'purchaseReturnQuantity','orderable':false  },
            { "name": 'salesQuantity','orderable':false  },
            { "name": 'salesReturnQuantity','orderable':false  },
            { "name": 'damageQuantity' ,'orderable':false },
            { "name": 'ongoingQuantity','orderable':false  },
            { "name": 'remainingQuantity','orderable':false  },
            { "name": 'action','orderable':false  }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [6], "sClass":  "text-center"}
        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        },
            {
                "targets": 0,
                "orderable": false
            }],

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });

     $('#hsCode').keyup(function(){
        dataTable.draw();
    });

    $('#brand').keyup(function(){
        dataTable.draw();
    });
    $('#productType').change(function(){
        dataTable.draw();
    });
    $('#masterName').change(function(){
        dataTable.draw();
    });
    $('#groupName').change(function(){
        dataTable.draw();
    });


});

