$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

    //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength":50, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url":  "/"+lang+"/nbrvat/vds/data-certificate-table", // ajax source
            'data': function(data){

                var created = $('#created').val();
                var invoice = $('#invoice').val();
                var company = $('#company').val();
                var challanNo = $('#challanNo').val();
                var total = $('#total').val();
                var balance = $('#balance').val();
                var amount = $('#amount').val();
                var process = $('#process').val();
                // Append to data
                //  data._token = CSRF_TOKEN;

                data.created = created;
                data.invoice = invoice;
                data.company = company;
                data.challanNo = challanNo;
                data.total = total;
                data.balance = balance;
                data.amount = amount;
                data.process = process;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'created' },
            { "name": 'invoice' },
            { "name": 'company' },
            { "name": 'challanNo'},
            { "name": 'total'},
            { "name": 'balance'},
            { "name": 'amount','orderable':false},
            { "name": 'process' },
            { "name": 'action','orderable':false,"sClass":  "text-left"}

        ],
        "order": [
            [1, "asc"]
        ]

    });


});

