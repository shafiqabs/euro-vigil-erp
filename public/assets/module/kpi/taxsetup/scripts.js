
$( document ).ready(function( $ ) {

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)')
            .exec(window.location.search);
        return (results !== null) ? results[1] || 0 : false;
    }

    $(document).on('opened', '.remodal', function () {
        var id = $.urlParam('process');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url);
    });

    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });


    $(document).on('click', '#addLocalProduct', function() {

        vdsApplicable = 0;
        rebateApplicable = 0;
        var productId = $('#productId').val();
        var purchaseInputTax = '';
        var quantity = $('#quantity').val();
        var purchasePrice = $('#purchasePrice').val();
        var vatPercent =  Number.parseFloat($('#vatPercent').val());
        var supplementaryDuty = $('#supplementaryDuty').val();
        if ($("#vdsApplicable:checked").length > 0 ) {
            var vdsApplicable = 1;
        }
        if ($("#rebateApplicable:checked").length > 0 ) {
            var rebateApplicable = 1;
        }

        var salesPrice = 0;
        var url = $(this).attr('data-action');
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        /*  if(purchaseInputTax === ''){
              $.MessageBox("Please select purchase input tax");
              $('#purchaseInputTax').focus();
              return false;
          }*/
        if(purchasePrice === ''){
            $.MessageBox("Please enter purchase price");
            $('#purchasePrice').focus();
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Please enter purchase quantity");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&purchaseInputTax='+purchaseInputTax+'&quantity='+quantity+'&purchasePrice='+purchasePrice+'&salesPrice='+salesPrice+'&vatPercent='+vatPercent+'&supplementaryDuty='+supplementaryDuty+'&vdsApplicable='+vdsApplicable+'&rebateApplicable='+rebateApplicable,
            success: function (response) {
                setTimeout(jsonResult(response),100);
                obj = JSON.parse(response);
                $('#purchasePrice').val('');
                $("#product").select2('open');
                $('#price').val('');
                $('#supplementaryDuty').val('');
                //  $('#purchaseInputTax').prop('selectedIndex',0);
                $('#unit').html('Unit');
                $('#quantity').val('');

            }
        })
    });

    $(document).on('change', '.action', function() {

        var id = $(this).attr('data-id');
        var url =  $('form#postForm').attr('data-action');
        var inputMode = $('#inputMode-'+id).val();
        var inputValue = $('#inputValue-'+id).val();
        var taxMode = $('#taxMode-'+id).val();
        $.ajax({
            "url":url,
            type: 'POST',
            data:'item='+ id +'&inputMode='+ inputMode+'&inputValue='+ inputValue+'&taxMode='+ taxMode,
            success: function(response) {

            }

        })
    });




});




