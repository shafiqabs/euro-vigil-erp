$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}
var pathname = window.location.pathname; // Returns path only (/path/example.html)
var url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
var origin   = window.location.origin;   // Returns base URL (https://example.com)


$( document ).ready(function( $ ) {

    $('#datepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('.datePicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('.timePicker').datetimepicker({
        format: 'LT'
    });

    $(document).on('click',".addVendor", function (event) {


        var url = $(this).attr('data-action');

        $.MessageBox({
            message : "<b>ADD VENDOR</b>",
            buttonDone      : {
                save : {
                    text        : "Save",
                    customClass : "custom_button",
                    keyCode     : 13
                }
            },
            buttonFail      : "Cancel",
            input   : {
                vendorType : {
                    type         : "select",
                    label        : "Vendor Type",
                    title        : "Select a vendor type",
                    options      : ["Local", "Foreign", "Production"]
                },
                businessType : {
                    type         : "select",
                    label        : "Vendor Business Type",
                    title        : "Select a vendor business type",
                    options      : ["General", "Group of Companies","Limited","Properties","Others"]
                },
                name    : {
                    type         : "text",
                    label        : "Vendor Name",
                    title        : "Enter Vendor Name"
                },
                company    : {
                    type         : "text",
                    label        : "Vendor Company",
                    title        : "Enter Vendor company name"
                },
                mobile    : {
                    type         : "text",
                    label        : "Vendor Mobile",
                    title        : "Enter Vendor mobile no"
                },
                binNo    : {
                    type         : "text",
                    label        : "Vendor BIN No",
                    title        : "Enter vendor BIN no"
                },
                vatRegistrationNo    : {
                    type         : "text",
                    label        : "Vendor vat registration no",
                    title        : "Enter vat registration no"
                }

            },
            filterDone      : function(data){
               alert(url);
                if (data['vendorType'] === null) return "Please fill the vendor type";
                if (data['name'] === "") return "Please fill vendor name";
                if (data['mobile'] === "") return "Please fill vendor mobile no";
                return $.ajax({
                    url     : url,
                    type    : "post",
                    data    : data
                }).then(function(response){
                    if (response == false) return "Wrong username or password";
                });
            },
            top     : "auto"
        }).done(function(data){
            console.log(data);
        });

    });

    $(document).on('change', '#product', function() {

        var url = $('#product').val();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                obj = JSON.parse(response);
                $('#productId').val(obj['productId']);
                $('#purchasePrice').val(obj['purchasePrice']);
                $('#unit').html(obj['unit']);
            }
        })
    });


    $(document).on('click', '#addProduct', function() {

        var productId = $('#productId').val();
        var quantity = $('#quantity').val();
        var price = $('#purchasePrice').val();
        var url = $('#addProduct').attr('data-action');
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        if(price === ''){
            $.MessageBox("Please enter purchase price");
            $('#purchasePrice').focus();
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Please enter purchase quantity");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&quantity='+quantity+'&price='+price,
            success: function (response) {
                obj = JSON.parse(response);
                $('#invoiceParticulars').html(obj['invoiceParticulars']);
                $('#subTotal').html(obj['subTotal']);
                $('#netTotal').html(obj['netTotal']);
                $('#paymentTotal').val(obj['netTotal']);
                $('#discount').html(obj['discount']);
                $('#due').html(obj['due']);
                $('#purchasePrice').val('');
                $("#particular").select2().select2("val","");
                $('#price').val('');
                $('#unit').html('Unit');
                $('#quantity').val('1');
            }
        })
    });


    $(document).on('click',".remove", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure you want to delete this record?"
        }).done(function(){
            $.get(url, function( data ) {
                $('#remove-'+id).hide();
                $(event.target).closest('tr').hide();
            });
        });
    });



    $(document).on('opened', '.remodal', function () {

        var id = $.urlParam('process');
        var check = $.urlParam('check');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url, function(){
            formCommonProcess();
            if(check === 'edit'){
                formEditSubmitProcess();
            }else{
                formSubmitProcess();
            }
        });
    });


    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });

 /*   $('.hover').tooltip({
        title: fetchData,
        html: true,
        placement: 'right'
    });*/
});

function formCommonProcess() {

    $('.form-body').slimScroll({
        height: '400px'
    });
    $('[data-toggle="tooltip"]').tooltip();

    $('.mobileLocal').mask("00000-000000", {placeholder: "_____-______"});

    $('.checkboxToggle').bootstrapToggle();

    $('.multi-select2').multiSelect({ selectableOptgroup: true });

    $('#optgroup').multiSelect({ selectableOptgroup: true });
    $('.select2').select2({
        theme: 'bootstrap4'
    });

    $('#makeEditable').SetEditable({ $addButton: $('#but_add')});

    $('#submit_data').on('click',function() {
        var td = TableToCSV('makeEditable', ',');
        console.log(td);
        var ar_lines = td.split("\n");
        var each_data_value = [];
        for(i=0;i<ar_lines.length;i++)
        {
            each_data_value[i] = ar_lines[i].split(",");
        }

        for(i=0;i<each_data_value.length;i++)
        {
            if(each_data_value[i]>1)
            {
                console.log(each_data_value[i][2]);
                console.log(each_data_value[i].length);
            }

        }

    });

}

var explode = function AutoReload()
{
    $('#entityDatatable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    });
    $('.form-submit').html("Save & Continue").prop("disabled", false);
}

function formSubmitProcess() {

    $("#postForm").validate({

        rules: {
            "customer_form[name]": {required: true},
            "customer_form[mobile]": {
                required: true,
                remote:window.location.pathname+"creatable/available"
            }
        },

        messages: {

            "customer_form[name]": "Enter user full name",
            "customer_form[mobile]":{
                required: "Please enter your mobile no.",
                remote: jQuery.validator.format("{0} username is already in use!")
            }
        },
        submitHandler: function(form) {
            $(".form-submit").prop("disabled", true);
            $.ajax({
                url         : $('form#postForm').attr( 'action' ),
                type        : $('form#postForm').attr( 'method' ),
                data        : new FormData($('form#postForm')[0]),
                processData : false,
                contentType : false,
                beforeSend: function() {
                    $('.form-submit').html("Loading...").attr('disabled', 'disabled');
                },
                success: function(response){
                    $('form#postForm')[0].reset();
                    $("#process-msg").show();
                    $(".alert-success").html(response);
                    setTimeout( explode, 2000);
                }
            });
        }
    });
}

function formEditSubmitProcess() {

    $("#postForm").validate({

        rules: {
            "customer_form[name]": {required: true},
            "customer_form[mobile]": {
                required: true,
                remote:window.location.pathname+"editable/available"
            }
        },

        messages: {
            "customer_form[name]": "Enter user full name",
            "customer_form[mobile]":{
                required: "Please enter your mobile no.",
                remote: jQuery.validator.format("{0} username is already in use!")
            }
        },
        submitHandler: function(form) {

            $(".form-submit").prop("disabled", true);
            $.ajax({
                url         : $('form#postForm').attr( 'action' ),
                type        : $('form#postForm').attr( 'method' ),
                data        : new FormData($('form#postForm')[0]),
                processData : false,
                contentType : false,
                beforeSend: function() {
                    $('.form-submit').html("Loading...").attr('disabled', 'disabled');
                },
                success: function(response){
                    $("#process-msg").show();
                    $(".alert-success").html(response);
                    setTimeout( explode, 2000);
                }
            });
        }
    });
}
