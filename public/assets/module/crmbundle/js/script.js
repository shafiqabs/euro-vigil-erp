//mortality percent

$('.mortality_pes, .totalBirds').on('keypress keyup blur',function () {
    var mortalityPes = $('.mortality_pes').val();
    var totalbirds= $('.totalBirds').val();
    if(mortalityPes!='' && totalbirds!=''){
        var calculateValue = (parseFloat(mortalityPes)*100)/parseFloat(totalbirds);
        $('.mortality_percent').val(calculateValue);
        $('.mortality_percent').text(calculateValue);
    }

});
//feed per bird

$('.feedTotalkg, .totalBirds').on('keypress keyup blur',function () {
    var totalbirds= $('.totalBirds').val();
    var feedTotalkg=$('.feedTotalkg').val();
    if(feedTotalkg != '' && totalbirds!=''){
        var calculation=(parseFloat(feedTotalkg)/(totalbirds))*(1000)
        $('.perBird').val(calculation);
        $('.perBird').text(calculation);
    }
});
//fcr without mortality

$('.feedTotalkg, .totalBirds, .weightAchieved').on('keypress keyup blur',function () {
    var totalbirds= $('.totalBirds').val();
    var feedTotalkg=$('.feedTotalkg').val();
    var weightAchieved=$('.weightAchieved').val();
    if((feedTotalkg != '' && totalbirds!='')&&(weightAchieved !='')){
        var cal=(parseFloat(feedTotalkg)/(totalbirds));
        var calculation=(parseFloat(cal)/weightAchieved)*1000;

        $('.withoutMortality').val(calculation);
        $('.withoutMortality').text(calculation);
    }
});

//fcr with mortality

$('.feedTotalkg, .totalBirds,.mortality_pes,.weightAchieved').on('keypress keyup blur',function () {
    var totalbirds= $('.totalBirds').val();
    var feedTotalkg=$('.feedTotalkg').val();
    var weightAchieved=$('.weightAchieved').val();
    var mortalityPes = $('.mortality_pes').val();

    if((feedTotalkg != '' && totalbirds!='')&&(weightAchieved !=''&& mortalityPes!='')){

        var cal=(parseFloat(feedTotalkg)/ (totalbirds-mortalityPes));
        var calculation=(parseFloat(cal)/weightAchieved)*1000;

        $('.withMortality').val(calculation);
        $('.withMortality').text(calculation);
    }
});


var count = 0;

$('.addmore').click(function(){

    var $el = $(this);
    var $cloneBlock = $('#clone-block');
    var $clone = $cloneBlock.find('.clone:eq(0)').clone();
    $clone.find('[id]').each(function(){this.id+='someotherpart'});
    $clone.find(':text,textarea' ).val("");
    $clone.attr('id', "added"+(++count));
    $clone.find('.row-remove').removeClass('hidden');
    $cloneBlock.prepend($clone);

});

$('#clone-block').on('click', '.row-remove', function(){
    $(this).closest('.clone').remove();
});


$('.addAgent').click(function(){
    var $el = $(this);
    var $cloneBlock = $('#agent-clone-block');
    var $clone = $cloneBlock.find('.agentClone:eq(0)').clone();
    $clone.find('[id]').each(function(){this.id+='someotherpart'});
    $clone.find(':text,textarea' ).val("");
    $clone.attr('id', "added"+(++count));
    $clone.find('.row-remove').removeClass('hidden');
    $cloneBlock.prepend($clone);

});

$('#agent-clone-block').on('click', '.row-remove', function(){
    $(this).closest('.agentClone').remove();
});

$('.addOtherAgent').click(function(){
    var $el = $(this);
    var $cloneBlock = $('#other-agent-clone-block');
    var $clone = $cloneBlock.find('.otherAgentClone:eq(0)').clone();
    $clone.find('[id]').each(function(){this.id+='someotherpart'});
    $clone.find(':text,textarea' ).val("");
    $clone.attr('id', "added"+(++count));
    $clone.find('.row-remove').removeClass('hidden');
    $cloneBlock.prepend($clone);

});

$('#other-agent-clone-block').on('click', '.row-remove', function(){
    $(this).closest('.otherAgentClone').remove();
});

$('.addSubAgent').click(function(){
    var $el = $(this);
    var $cloneBlock = $('#sub-agent-clone-block');
    var $clone = $cloneBlock.find('.subAgentClone:eq(0)').clone();
    $clone.find('[id]').each(function(){this.id+='someotherpart'});
    $clone.find(':text,textarea' ).val("");
    $clone.attr('id', "added"+(++count));
    $clone.find('.row-remove').removeClass('hidden');
    $cloneBlock.prepend($clone);

});

$('#sub-agent-clone-block').on('click', '.row-remove', function(){
    $(this).closest('.subAgentClone').remove();
});


$(document).on('click', '.meta-remove', function(){
    var id = $(this).attr('data-id');
    var url = $(this).attr('data-action');
    $.MessageBox({
        buttonFail  : "No",
        buttonDone  : "Yes",
        message     : "Are you sure want to delete this record?"
    }).done(function(){
        $.get(url, function( data ) {
            $('remove-'+id).remove();
            $(this).closest('.clone-remove').remove();
        });
    });
});


$(document).on('click', '#crm-farmer-btn', function(e) {

    e.preventDefault();
    var name =$(this).closest("form").find(".name").val();
    var mobile = $(this).closest("form").find(".mobile").val();

    if (name === "") {
        alert("Name must be filled out");
        return false;
    }
    else if(mobile==="" || mobile ===null){
        alert("Your mobile number is Invalid :" +mobile);
        return false;
    }

    $.ajax({
        url         : $('form#farmerForm').attr( 'action' ),
        type        : $('form#farmerForm').attr( 'method' ),
        data        : new FormData($('form#farmerForm')[0]),
        processData : false,
        contentType : false,
        success: function (data) {
            $('form#farmerForm')[0].reset();
        }
    });

});

$(document).on('click', '#crm-other-agent-btn', function(e) {

    e.preventDefault();
    var name =$(this).closest("form").find(".name").val();
    var mobile = $(this).closest("form").find(".mobile").val();

    if (name === "") {
        alert("Name must be filled out");
        return false;
    }
    else if(mobile==="" || mobile ===null){
        alert("Your mobile number is Invalid :" +mobile);
        return false;
    }
    var form= $("#otherAgent");
    $.ajax({
        url         : $('form#otherAgentForm').attr( 'action' ),
        type        : $('form#otherAgentForm').attr( 'method' ),
        data        : new FormData($('form#otherAgentForm')[0]),
        processData : false,
        contentType : false,
        success: function (data) {
            $('form#otherAgentForm')[0].reset();
        }
    });

});

$(document).on('click', '#crm-sub-agent-btn', function(e) {

    e.preventDefault();
    var name =$(this).closest("form").find(".name").val();
    var mobile = $(this).closest("form").find(".mobile").val();

    if (name === "") {
        alert("Name must be filled out");
        return false;
    }
    else if(mobile==="" || mobile ===null){
        alert("Your mobile number is Invalid :" +mobile);
        return false;
    }
    var form= $("#subAgent");
    $.ajax({
        url         : $('form#subAgentForm').attr( 'action' ),
        type        : $('form#subAgentForm').attr( 'method' ),
        data        : new FormData($('form#subAgentForm')[0]),
        processData : false,
        contentType : false,
        success: function (data) {
            $('form#subAgentForm')[0].reset();
        }
    });

});

