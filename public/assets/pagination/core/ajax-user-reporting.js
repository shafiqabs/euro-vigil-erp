$(document).ready(function () {

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": Routing.generate('core_reporting_data_table'), // ajax source
            'data': function(data){

                var name = $('#name').val();
                var branch = $('#branch').val();
                var designation = $('#designation').val();
                var operationalDesignation = $('#operationalDesignation').val();
                var department = $('#department').val();
                var reportTo = $('#reportTo').val();
                var reliever = $('#reliever').val();
                var status = $('#status').val();

                data.name = name;
                data.branch = branch;
                data.designation = designation;
                data.operationalDesignation = operationalDesignation;
                data.department = department;
                data.reportTo = reportTo;
                data.reliever = reliever;
                data.status = status;
            }
        },
        "initComplete": function() {
            $('.status').bootstrapToggle();
        },
        'columns': [
            { "name": 'id' },
            { "name": 'name' },
            { "name": 'branch' },
            { "name": 'designation' },
            { "name": 'operationalDesignation' },
            { "name": 'department' },
            { "name": 'reportTo' },
            { "name": 'reliever' },
            { "name": 'status' },
            { "name": 'action' }

        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        },
        {
            "targets": 0,
            "orderable": false
        }]

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });

    $('#branch').change(function(){
        dataTable.draw();
    });

    $('#department').change(function(){
        dataTable.draw();
    });

    $('#designation').change(function(){
        dataTable.draw();
    });

    $('#reportTo').keyup(function(){
        dataTable.draw();
    });

    $('#reliever').keyup(function(){
        dataTable.draw();
    });

    $('#status').change(function(){
        dataTable.draw();
    });

    $("#csvBtn").on("click", function() {
        dataTable.button( '.buttons-csv' ).trigger();
    });

    $("#printBtn").on("click", function() {
        dataTable.button( '.buttons-print' ).trigger();
    });

    $("#excelBtn").on("click", function() {
        dataTable.button( '.buttons-excel' ).trigger();
    });

    $("#pdfBtn").on("click", function() {
        dataTable.button( '.buttons-pdf' ).trigger();
    });

    $('#entityDatatable').editable({
        container:'body',
        selector:'a.editable'
    });

});

