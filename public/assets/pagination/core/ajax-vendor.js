$(document).ready(function () {

    var dataTable = $('#datatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': true,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "initComplete": function() {
            $('.status').bootstrapToggle()
        },
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": Routing.generate('domain_vendor_data_table'), // ajax source
            'data': function(data){

                var businessType = $('#businessType').val();
                var vendorType = $('#vendorType').val();
                var company = $('#company').val();
                var name = $('#name').val();
                var mobile = $('#mobile').val();
                var location = $('#location').val();
                var vatRegistrationNo = $('#vatRegistrationNo').val();
                var status = $('#status').val();

                // Append to data
                data.businessType = businessType;
                data.vendorType = vendorType;
                data.company = company;
                data.name = name;
                data.mobile = mobile;
                data.location = location;
                data.vatRegistrationNo = vatRegistrationNo;
                data.status = status;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'businessType' },
            { "name": 'vendorType' },
            { "name": 'company' },
            { "name": 'name' },
            { "name": 'mobile' },
            { "name": 'location' },
            { "name": 'vatRegistrationNo' },
            { "name": 'status' },
            { "name": 'action' }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [7], "sClass":  "text-center"}
        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {orderable: false,targets:7},{orderable: false,targets:8}],

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#businessType').change(function(){
        dataTable.draw();
    });

     $('#vendorType').change(function(){
        dataTable.draw();
    });

    $('#companyName').keyup(function(){
        dataTable.draw();
    });

    $('#mobile').keyup(function(){
        dataTable.draw();
    });

    $('#email').change(function(){
        dataTable.draw();
    });

});

