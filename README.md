Euro Vigil Application
========================

Created BY
-----------
* Md Shafiqul Islam
* shafiq@rightbrainsolution.com
* Powered by Right Brain Solution Ltd.

Requirements
------------

  * PHP 7.2.9 or higher;
  * PDO-SQLite PHP extension enabled;
  * and the [usual Symfony application requirements][2].

Installation
------------

