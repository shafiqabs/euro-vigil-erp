<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 4/9/15
 * Time: 5:05 PM
 */

namespace App\EventListener;
use App\Service\EasyMailer;


abstract class BaseEmailAwareListener {

    /**
     * @var EasyMailer
     */
    protected $easymailer;

    public function  __construct(EasyMailer $easymailer)
    {
        $this->easymailer = $easymailer;
    }

}