<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Admin;


use App\Entity\Admin\AppBundle;
use App\Entity\Admin\AppModule;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class AppmoudleFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true,
                'label' => 'label.name',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Enter application module name',
                    ]),
                ],
            ])
             ->add('slug', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true,
                'label' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Enter application module slug',
                    ]),
                ],
            ])
            ->add('appBundle', EntityType::class, [
                'class' => AppBundle::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please choose a app bundle',
                    ]),
                ],
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a app bundle',
            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AppModule::class,
        ]);
    }
}
