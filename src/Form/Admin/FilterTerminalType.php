<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace App\Form\Admin;

use App\Entity\Admin\AppBundle;
use App\Entity\Admin\AppModule;
use App\Entity\Admin\Setting;
use App\Entity\Admin\Terminal;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class FilterTerminalType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true , 'class' => '','placeholder'=>'Enter organization name'],
                'required' => false,
                'mapped'=> false,
            ])
            ->add('mobile', TextType::class, [
                'attr' => ['autofocus' => true , 'class' => '','placeholder'=>'Enter mobile no'],
                'required' => false,
                'mapped'=> false
            ])
            ->add('address', TextareaType::class, [
                'attr' => ['autofocus' => true,'class' => 'address'],
                'required' => false,
            ])
              ->add('domain', TextType::class, [
                   'attr' => ['autofocus' => true],
                  'required' => false,
                  'mapped'=> false,
               ])
               ->add('mainApp', EntityType::class, [
                   'class' => AppBundle::class,
                   'required' => false,
                   'mapped'=> false,
                   'query_builder' => function (EntityRepository $er) {
                       return $er->createQueryBuilder('e')
                           ->orderBy('e.name', 'ASC');
                   },
                   'attr'=>['class'=>'select2'],
                   'choice_label' => 'name',
                   'placeholder' => 'Choose a main app',
               ])
               ->add('businessGroup', EntityType::class, [
                    'class' => Setting::class,
                   'required' => false,
                   'mapped'=> false,
                    'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","t")
                        ->where("e.status = 1")
                        ->andWhere("t.slug = 'business-group'")
                        ->orderBy('e.name', 'ASC');
                    },
                    'attr'=>['class'=>'span12 select2'],
                    'choice_label' => 'name',
                    'placeholder' => 'Choose a business group',
                ])->add('search', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-sm  blue-bg white-font'
                ]
            ])  ->setMethod('GET')
            ->getForm()
        ;



    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

        ]);
    }


}