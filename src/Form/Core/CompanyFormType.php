<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Core;



use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\AccountInformation;
use App\Entity\Core\CompanyRegistration;
use App\Entity\Core\Customer;
use App\Entity\Core\Setting;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class CompanyFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('registrationCategory', ChoiceType::class, [
                'choices'  => ['New' => 'new','Re-registration' => 'Re-registration'],
                'required'    => true,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process action'],
            ])
            ->add('companyName', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('oldBin', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('businessOwnership', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'business-type')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a business type',
            ])
            ->add('economicActivity', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'economic-activity')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a economic activity',
            ])
            ->add('otherOwnershipType', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false,
                    'help' => 'Please specify in the field below',
            ])
            ->add('holdingEntity',CheckboxType::class,[
                'required' => false,
                'label' => "B.2 Are you a Withholding Entity",
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Yes",
                    'data-off'=> "No"
                ],
            ])
            ->add('tradeLicense', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Trade License No'],
                'required' => false
            ])
            ->add('tradeLicenseIssueDate', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Trade License Issue Date','class'=>'datePicker col-md-6'],
                'required' => false,
            ])
            ->add('incorporationNo', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter RJSC Incorporation No'],
                'required' => false
            ])
            ->add('incorporationIssueDate', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter RJSC Issue Date','class'=>'datePicker col-md-6'],
                'required' => false,
            ])
            ->add('eTin', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter e-TIN '],
                'required' => false
            ])
             ->add('eTinNameOfCompany', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter e-TIN name of company '],
                'required' => false
            ])
            ->add('differentEtin', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter different e-TIN of the company '],
                'required' => false,
                'help' => '(Where applicable)',
            ])
            ->add('tradingBrandName', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Trading Brand Name'],
                'required' => false
            ])
            ->add('registrationType', ChoiceType::class, [
                'choices'  => ['VAT' => 'VAT','Turnover Tax' => 'Turnover Tax'],
                'required'    => true,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process action'],
            ])
            ->add('equityInformation', ChoiceType::class, [
                'choices'  => ['100% Local' => '100% Local','100% Foreign' => '100% Foreign','Joint Venture' => 'Joint Venture'],
                'required'    => true,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process action'],
            ])

            ->add('equityLocalShare', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter local share'],
                'required' => false
            ])
            ->add('bidaRegistrationNo', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter BIDA Registration No'],
                'required' => false
            ])
            ->add('bidaIssueDate', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter BIDA Issue Date','class'=>'datePicker col-md-6'],
                'required' => false,
            ])
            ->add('companyOperationAddress', TextareaType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Company Operation Address','class'=>'textarea'],
                'required' => false,
            ])
            ->add('district', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter District Name','class'=>''],
                'required' => false,
            ])
            ->add('policeStation', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Police Station','class'=>''],
                'required' => false,
            ])
            ->add('postCode', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Postal Code','class'=>''],
                'required' => false,
            ])
            ->add('landPhoneNo', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Company Land Phone','class'=>''],
                'required' => false,
                'help' => '(District code+phone number)',
            ])
            ->add('mobile', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Company Mobile No','class'=>''],
                'required' => false,
                'help' => '(e.g 01XXXXXXXXX)',
            ])
            ->add('email', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Company Email Address','class'=>''],
                'required' => false,
            ])
            ->add('faxNumber', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Company Fax No','class'=>''],
                'required' => false,
                'help' => '(District code+phone number)',
            ])
            ->add('webAddress', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Company Web Address','class'=>''],
                'required' => false,
            ])
            ->add('headQuarterAddress', TextareaType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Company Head Quarter Address','class'=>'textarea'],
                'required' => false,
            ])
            ->add('headQuarterAddressOutside', TextareaType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Company Head Quarter Address Outside','class'=>'textarea'],
                'required' => false,
            ])
            ->add('isManufacturing', CheckboxType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'css-checkbox',
                ],
                'label' => 'F1. Manufacturing',
                'required' => false
            ])
            ->add('isServicing', CheckboxType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'css-checkbox',
                ],
                'label' => 'F2. Services',
                'required' => false
            ])
            ->add('isImport', CheckboxType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'css-checkbox',
                ],
                'label' => 'F3. Imports',
                'required' => false
            ])
            ->add('ircNumber', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter IRC Number','class'=>''],
                'required' => false,
            ])
            ->add('ircIssueDate', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter IRC Issue Date','class'=>''],
                'required' => false,
            ])
            ->add('isExport', CheckboxType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'css-checkbox',
                ],
                'label' => 'F4. Exports',
                'required' => false
            ])
            ->add('ercNumber', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter ERC Number','class'=>''],
                'required' => false,
            ])
            ->add('ercIssueDate', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter ERC Issue Date','class'=>''],
                'required' => false,
            ])
            ->add('isEconomicActivityOther', CheckboxType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'css-checkbox',
                ],
                'label' => 'F5. Other',
                'required' => false
            ])
            ->add('economicActivityOther', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter ERC Number','class'=>''],
                'required' => false,
            ])
            ->add('manufacturingArea', EntityType::class, [
                'class' => Setting::class,
                'required'      => true,
                'expanded'      =>true,
                'multiple'      =>true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'business-type')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'css-checkbox'],
                'choice_label' => 'name',
            ])
            ->add('manufacturingOther', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter ERC Number','class'=>''],
                'required' => false,
            ])
            ->add('servicingArea', EntityType::class, [
                'class' => Setting::class,
                'required'      => true,
                'expanded'      =>true,
                'multiple'      =>true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'business-type')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'css-checkbox'],
                'choice_label' => 'name',
            ])
            ->add('servicingOther', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter ERC Number','class'=>''],
                'required' => false,
            ])
            ->add('taxableTurnover', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Taxable Turnover','class'=>''],
                'required' => false,
            ])
            ->add('projectedTurnover', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter Projected Turnover','class'=>''],
                'required' => false,
            ])
            ->add('noOfEmployee', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'Enter No of Employee','class'=>''],
                'required' => false,
            ])
            ->add('zeroRatedSupply', CheckboxType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'css-checkbox',
                ],
                'label' => 'L4. Are you making any zero rated supply',
                'required' => false
            ])

            ->add('vatExemptedSupply', CheckboxType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'css-checkbox',
                ],
                'label' => 'L5. Are you making any VAT Exempted supply',
                'required' => false
            ])


        ;
    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompanyRegistration::class,
        ]);
    }
}
