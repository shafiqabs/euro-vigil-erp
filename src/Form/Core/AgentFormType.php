<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Form\Core;



use App\Entity\Admin\Location;
use App\Entity\Core\AccountInformation;
use App\Entity\Core\Agent;
use App\Entity\Core\Agents;
use App\Entity\Core\Customer;
use App\Entity\Core\Setting;
use App\Repository\Admin\LocationRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class AgentFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('mobile', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'span12  mobileLocal'],
                'required' => true
            ])
            ->add('binNo', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
             ->add('nid', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
            ->add('address', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])

            ->add('upozila', EntityType::class, [
                'class' => Location::class,
                'attr'=>['class'=>'span12'],
                'placeholder' => 'Choose a thana',
                'choice_label' => 'nestedLabel',
                'choices'   => $options['locationRepo']->getUpozilaOptionGroup()
            ])

            ->add('agentGroup', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'agent-group')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a agent group',
            ])
             ->add('phone', TextType::class, [
                'attr' => ['autofocus' => true],
                 'required' => false
            ])
             ->add('email', TextType::class, [
                'attr' => ['autofocus' => true],
                 'required' => false
            ]);
        // $builder->add('accountInfo', AccountInformationFormType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Agent::class,
            'locationRepo' => LocationRepository::class,
        ]);
    }
}
