<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Domain;



use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use App\Entity\Core\Setting;
use App\Entity\Domain\Vendor;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class VendorFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('mobile', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                'required' => true
            ])
            ->add('companyName', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('branchName', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
            ->add('accountName', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
            ->add('accountNo', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
            ->add('accountType', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
            ->add('vatRegistrationNo', TextType::class, [
                'attr' => ['autofocus' => false, 'placeholder' => "Enter VAT/BIN no "],
                'required' => true
            ])
            ->add('address', TextareaType::class, [
                'attr' => ['autofocus' => false, 'placeholder' => "Enter vendor address"],
                'required' => true
            ])
            ->add('bank', EntityType::class, [
                'class' => Bank::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a bank name',
            ])
            ->add('location', EntityType::class, [
                'class' => Location::class,
                'required' => false,
                'group_by'  => 'parent.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.level =3')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a location name',
            ])
            ->add('businessType', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'business-type')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a business type',
            ])
            ->add('vendorType', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'vendor-type')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a vendor type',
            ])
             ->add('email', TextType::class, [
                'attr' => ['autofocus' => true],
                 'required' => false
            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;
     //   $builder->add('accountInfo', AccountInformationFormType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Vendor::class,
        ]);
    }
}
