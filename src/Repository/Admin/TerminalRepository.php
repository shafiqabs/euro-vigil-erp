<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Admin;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Application\Assets;
use App\Entity\Application\Billing;
use App\Entity\Application\Budget;
use App\Entity\Application\Crm;
use App\Entity\Application\Dms;
use App\Entity\Application\Garments;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Inventory;
use App\Entity\Application\Kpi;
use App\Entity\Application\Procurement;
use App\Entity\Application\Production;
use App\Entity\Application\SecurityBilling;
use App\Entity\User;
use App\Service\ConfigureManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class TerminalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Terminal::class);
    }

    public function recordCount()
    {
        return $count = $this->createQueryBuilder('e')
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function checkAvailable($mode,$data)
    {
        $process = "true";
        $organizationName   = isset($data['organizationName']) ? $data['organizationName'] :'';
        $mobile             = isset($data['mobile']) ? $data['mobile'] :'';
        $domain             = isset($data['domain']) ? $data['domain'] :'';
        $subDomain          = isset($data['subDomain']) ? $data['subDomain'] :'';

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        if(!empty($organizationName)){
            $qb->andWhere('e.organizationName =:main')->setParameter("main",$organizationName);
        }
        if(!empty($mobile)){
            $confManager = new ConfigureManager();
            $confManager->specialExpClean($mobile);
            $qb->andWhere('e.mobile =:mobile')->setParameter("mobile",$mobile);
        }
        if(!empty($domain)){
            $qb->andWhere('e.domain =:domain')->setParameter("domain",$domain);
        }
        if(!empty($subDomain)){
            $qb->andWhere('e.subDomain =:subDomain')->setParameter("subDomain",$subDomain);
        }
        $count = $qb->getQuery()->getOneOrNullResult();
        if($mode == "creatable"){
            if ($count['count'] == 1 ){
                $process="false";
            }
        }elseif($mode == "editable") {
            if ($count['count'] > 1 ){
                $process="false";
            }
        }
        return $process;

    }

    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['filter_terminal'])){
            $data = $form['filter_terminal'];
            $created = isset($data['created'])? $data['created'] :'';
            $organizationName = isset($data['organizationName'])? $data['organizationName'] :'';
            $name = isset($data['name'])? $data['name'] :'';
            $mobile = isset($data['mobile'])? $data['mobile'] :'';
            $location = isset($data['location'])? $data['location'] :'';
            $mainApp = isset($data['mainApp'])? $data['mainApp'] :'';
            $businessGroup = isset($data['businessGroup'])? $data['businessGroup'] :'';
            if(!empty($name)){
                $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
            }
            if(!empty($organizationName)){
                $qb->andWhere($qb->expr()->like("e.organizationName", "'%$organizationName%'"));
            }
            if(!empty($mobile)){
                $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
            }
            if(!empty($location)){
                $qb->andWhere($qb->expr()->like("l.name", "'%$location%'"));
            }
            if(!empty($mainApp)){
                $qb->andWhere('e.mainApp =:main')->setParameter("main",$mainApp);
            }
            if(!empty($businessGroup)){
                $qb->andWhere('e.businessGroup =:syndicate')->setParameter("syndicate",$businessGroup);
            }
            if (!empty($created) ) {
                $datetime = new \DateTime($created);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate")->setParameter('startDate', $startDate);
                $date = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate")->setParameter('endDate', $date);
            }
        }


    }

    /**
     * @return Terminal[]
     */
    public function findBySearchQuery($data): array
    {

        $qb = $this->_em->createQueryBuilder();
        $qb->from(User::class,'user');
        $qb->join('user.terminal','e');
        $qb->leftJoin('e.mainApp','a');
        $qb->leftJoin('e.businessGroup','s');
        $qb->leftJoin('e.location','l');
        $qb->select('e.id as id','e.created as created','e.name as name','e.name as organization','e.mobile as mobile','e.status as status');
        $qb->addSelect('a.name as mainApp');
        $qb->addSelect('l.name as location');
        $qb->addSelect('s.name as syndicateName');
        $qb->addSelect('user.username as username','user.id as userId');
        $qb->where('user.isDomain = 1');
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy('e.id', 'DESC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function setupModule(Terminal $terminal )
    {

        $em = $this->_em;
        $term = $terminal->getId();

        $Crm = $em->getRepository(Crm::class)->findOneBy(array('terminal'=> $term));
        if(empty($Crm)){
            $config = new Crm();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $inventory = $em->getRepository(Inventory::class)->findOneBy(array('terminal'=> $term));
        if(empty($inventory)){
            $config = new Inventory();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $Kpi = $em->getRepository(Kpi::class)->findOneBy(array('terminal'=>$term));
        if(empty($Kpi)){
            $config = new Kpi();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $SecurityBilling = $em->getRepository(SecurityBilling::class)->findOneBy(array('terminal'=>$term));
        if(empty($SecurityBilling)){
            $config = new SecurityBilling();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $Procurement = $em->getRepository(Procurement::class)->findOneBy(array('terminal'=>$term));
        if(empty($Procurement)){
            $config = new Procurement();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $Accounting = $em->getRepository(Accounting::class)->findOneBy(array('terminal'=> $term));
        if(empty($Accounting)){
            $config = new Accounting();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $Assets = $em->getRepository(Assets::class)->findOneBy(array('terminal'=>$term));
        if(empty($Assets)){
            $config = new Assets();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $Budget = $em->getRepository(Budget::class)->findOneBy(array('terminal'=>$term));
        if(empty($Budget)){
            $config = new Budget();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $Billing = $em->getRepository(Billing::class)->findOneBy(array('terminal'=> $term));
        if(empty($Billing)){
            $config = new Billing();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $production = $em->getRepository(Production::class)->findOneBy(array('terminal'=>$term));
        if(empty($production)){
            $config = new Production();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $generic = $em->getRepository(GenericMaster::class)->findOneBy(array('terminal'=>$term));
        if(empty($generic)){
            $config = new GenericMaster();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $dms = $em->getRepository(Dms::class)->findOneBy(array('terminal'=>$term));
        if(empty($dms)){
            $config = new Dms();
            $config->setTerminal($term);
            $em->persist($config);
        }

        $garments = $em->getRepository(Garments::class)->findOneBy(array('terminal'=>$term));
        if(empty($garments)){
            $config = new Garments();
            $config->setTerminal($term);
            $em->persist($config);
        }
        $em->flush();

    }


}
