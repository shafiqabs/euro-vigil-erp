<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Admin;

use App\Entity\Admin\AppModule;
use App\Entity\Admin\Terminal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class AppModuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppModule::class);
    }

    public function getSpecificModule(Terminal $terminal)
    {
        $appbundles = $terminal->getAppBundles();
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.appBundle IN (:bundles)')->setParameter('bundles',$appbundles);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

}
