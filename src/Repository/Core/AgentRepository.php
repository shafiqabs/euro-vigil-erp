<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Core;
use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\Agent;
use App\Entity\Core\Setting;
use App\Entity\User;
use App\Service\ConfigureManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class AgentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Agent::class);
    }

    public function getLocationWise(User $user)
    {

        $arrs = array();
        foreach ($user->getUpozila() as $location){
            $arrs[] = $location->getId();
        }

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.upozila','location');
        $qb->select('e.id as id','e.name as name');
        $qb->andWhere('location.id IN (:upozils)')->setParameter('upozils',$arrs);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }


    public function getLocationWiseAgentForm(User $user)
    {

        $arrs = array();

        foreach ($user->getUpozila() as $location){
            $arrs[] = $location->getId();
        }
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.upozila','location');
        $qb->where('location.id IN (:upozils)')->setParameter('upozils',$arrs);
        $result = $qb->getQuery()->getResult();
        return $result;

    }


    public function totalCount()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $count = $qb->getQuery()->getSingleScalarResult();
        return $count;
    }

    public function checkAvailable($terminal , $mode,$data)
    {
        $process = "true";
        $mobile             = isset($data['mobile']) ? $data['mobile'] :'';

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->where('e.terminal =:terminal')->setParameter("terminal",$terminal);
        if(!empty($mobile)){
            $confManager = new ConfigureManager();
            $confManager->specialExpClean($mobile);
            $qb->andWhere('e.mobile =:mobile')->setParameter("mobile",$mobile);
        }
        $count = $qb->getQuery()->getOneOrNullResult();
        if($mode == "creatable"){
            if ($count['count'] == 1 ){
                $process="false";
            }
        }elseif($mode == "editable") {
            if ($count['count'] > 1 ){
                $process="false";
            }
        }
        return $process;

    }

    protected function handleSearchBetween($qb,$data)
    {

        $name = isset($data['name'])? $data['name'] :'';
        $username = isset($data['username'])? $data['username'] :'';
        $email = isset($data['email'])? $data['email'] :'';
        $mobile = isset($data['mobile'])? $data['mobile'] :'';

        if(!empty($name)){
            $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
        }
        if(!empty($username)){
            $qb->andWhere($qb->expr()->like("e.username", "'%$username%'"));
        }
        if(!empty($email)){
            $qb->andWhere($qb->expr()->like("e.email", "'%$email%'"));
        }
        if(!empty($mobile)){
            $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
        }

    }

    public function systemDefaultCustomer( Terminal $terminal){

        $em = $this->_em;
        $user = new Agent();
        $user->setName("Default");
        $user->setMobile($terminal->getMobile());
        if($terminal->getEmail()){
            $user->setEmail($terminal->getEmail());
        }
        $em->persist($user);
        $em->flush();
    }


    /**
     * Agent
     */
    public function findBySearchQuery( $terminal, $parameter , $data ): array
    {


        if (!empty($parameter['orderBy'])) {
            $sortBy = $parameter['orderBy'];
            $order = $parameter['order'];
        }

        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.agentGroup','bt');
        $qb->leftJoin('e.upozila','l');
        $qb->leftJoin('l.parent','p');
        $qb->select('e.id as id','e.name as name','e.mobile as mobile','e.email as email','e.name as companyName','e.agentId as agentId');
        $qb->addSelect('bt.name as businessType');
        $qb->addSelect('l.name as thana');
        $qb->addSelect('p.name as district');
       // $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        //    $this->handleSearchBetween($qb,$data);
        $qb->setFirstResult($parameter['offset']);
        $qb->setMaxResults($parameter['limit']);
        if ($parameter['orderBy']){
            $qb->orderBy($sortBy, $order);
        }else{
            $qb->orderBy('e.id', 'DESC');
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function apiInsert($data)
    {
        $em = $this->_em;
        foreach ($data as $key => $value){
            $date = new \DateTime($data[$key]['created']);
            $entity = new Agent();
            $entity->setOldId($data[$key]['id']);
            $entity->setAgentId($data[$key]['agentId']);
            $entity->setName($data[$key]['company']);
            $entity->setAddress($data[$key]['address']);
            $entity->setMobile($data[$key]['phone']);
            $entity->setEmail($data[$key]['email']);
            $entity->setCreated($date);
            if($data[$key]['agentType']){
                $slug = strtolower($data[$key]['agentType']);
                $group = $em->getRepository(Setting::class)->findOneBy(array('slug' => $slug));
                $entity->setAgentGroup($group);
            }
            if($data[$key]['upozila']){
                $upozila = trim($data[$key]['upozila']);
                $location = $em->getRepository(Location::class)->findOneBy(array('oldId' => $upozila));
                $entity->setUpozila($location);
                $entity->setDistrict($location->getParent());
            }
            $em->persist($entity);
            $em->flush();
        }
    }

}
