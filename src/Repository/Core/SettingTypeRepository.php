<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Core;


use App\Entity\Core\SettingType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class SettingTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettingType::class);
    }

    public function getGroups($arrs = [])
    {
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.app = :app')->setParameter('app', 1);
        $qb->andWhere('e.slug IN (:slugs)')->setParameter('slugs',$arrs);
        $result  = $qb->getQuery()->getResult();
        return $result;

    }



}
