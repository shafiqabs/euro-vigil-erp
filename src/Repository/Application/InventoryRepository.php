<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Application;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockBranch;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\InventoryBundle\Entity\WearHouse;
use Terminalbd\ProcurementBundle\Entity\Requisition;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class InventoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inventory::class);
    }

    public function config($terminal)
    {
        $config = $this->findOneBy(array('terminal' => $terminal));
        return $config;
    }

    public function reset($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));

        if($con) {
            $config = $con->getId();
            $qb = $em->createQueryBuilder();
            $StockHistory = $qb->delete(StockHistory::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($StockHistory){ $StockHistory->execute();}
            $purchase = $qb->delete(Purchase::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($purchase){ $purchase->execute();}
            $StockWearhouse = $qb->delete(StockWearhouse::class, 'e')
                ->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($StockWearhouse){
                $StockWearhouse->execute();
            }

            $ids = $this->_em->createQueryBuilder()
                ->from(StockWearhouse::class,'e')
                ->join('e.stock','stock')
                ->where("stock.config={$config}")
                ->select('e.id')
                ->getQuery()->getResult();

            $this->_em->createQueryBuilder()
                ->from(StockWearhouse::class,'e')
                ->where('e.id in (:ids)')
                ->setParameter('ids', $ids)
                ->delete()
                ->getQuery()
                ->execute();

            $StockBranch = $qb->delete(WearHouse::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($StockBranch){
                $StockBranch->execute();
            }
            $queryBuilder = $this->_em->createQueryBuilder();
            $query = $queryBuilder ->update(StockBook::class, 'e')
                ->set('e.stockIn', ':stockIn')
                ->setParameter('stockIn', 0)
                ->set('e.stockOut', ':stockOut')
                ->setParameter('stockOut', 0)
                ->set('e.remainingQuantity', ':remaining')
                ->setParameter('remaining', 0)
                ->where('e.config =:config')
                ->setParameter('config', $config)
                ->getQuery();
            $query ->execute();

            $query2 = $queryBuilder ->update(Stock::class, 'e')
                ->set('e.stockIn', ':stockIn')
                ->setParameter('stockIn', 0)
                ->set('e.stockOut', ':stockOut')
                ->setParameter('stockOut', 0)
                ->set('e.remainingQuantity', ':remaining')
                ->setParameter('remaining', 0)
                ->where('e.config =:config')
                ->setParameter('config', $config)
                ->getQuery();
            $query2->execute();

        }
    }
    public function remove($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));

        if($con) {
            $config = $con->getId();
            $qb = $em->createQueryBuilder();
            $StockHistory = $qb->delete(StockHistory::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($StockHistory){ $StockHistory->execute();}
            $purchase = $qb->delete(Purchase::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($purchase){ $purchase->execute();}
            $StockWearhouse = $qb->delete(StockWearhouse::class, 'e')
                ->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($StockWearhouse){
                $StockWearhouse->execute();
            }
            $ids = $this->_em->createQueryBuilder()
                ->from(StockWearhouse::class,'e')
                ->join('e.stock','stock')
                ->where("stock.config={$config}")
                ->select('e.id')
                ->getQuery()->getResult();
            $this->_em->createQueryBuilder()
                ->from(StockWearhouse::class,'e')
                ->where('e.id in (:ids)')
                ->setParameter('ids', $ids)
                ->delete()
                ->getQuery()
                ->execute();
            $StockBook = $qb->delete(StockBook::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($StockBook){ $StockBook->execute();}
            $Stock = $qb->delete(Stock::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Stock){ $Stock->execute();}
            $WearHouse = $qb->delete(WearHouse::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($WearHouse){ $WearHouse->execute();}
        }
        if(!empty($con) and $process == "remove"){
            $em->remove($con);
            $em->flush();
        }

    }



}
