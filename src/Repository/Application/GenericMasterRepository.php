<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Application;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Kpi;
use App\Entity\Application\Nrbvat;
use App\Entity\Application\Production;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\CategoryMeta;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\GenericBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class GenericMasterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GenericMaster::class);
    }

    public function config($terminal)
    {
        $config = $this->findOneBy(array('terminal' => $terminal));
        return $config;
    }

    public function reset($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));
        if($con) {

            $qb = $em->createQueryBuilder();
            $config = $con->getId();

        }

    }


    public function remove($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));
        if($con) {

            $qb = $em->createQueryBuilder();
            $config = $con->getId();

            $Item = $qb->delete(Item::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Item){
                $Item->execute();
            }

            $ItemColor = $qb->delete(ItemColor::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($ItemColor){
                $ItemColor->execute();
            }

            $ItemSize = $qb->delete(ItemSize::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($ItemSize){
                $ItemSize->execute();
            }

            $ItemBrand = $qb->delete(ItemBrand::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($ItemBrand){
                $ItemBrand->execute();
            }

            $Category = $qb->delete(Category::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Category){
                $Category->execute();
            }

            $Particular = $qb->delete(Particular::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Particular){
                $Particular->execute();
            }

        }
        if($con and $process == "remove"){
            $em->remove($con);
            $em->flush();
        }

    }

}
