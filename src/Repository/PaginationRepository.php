<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;


use App\Entity\Admin\AppBundle;
use App\Entity\Admin\AppModule;
use App\Entity\Admin\Terminal;
use App\Entity\Domain\BundleRoleGroup;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class PaginationRepository extends EntityRepository
{
    public function __construct()
    {
        parent::__construct();
    }
    protected function handleSearchBetween($qb,$form)
    {

        if (isset($form['filter_form'])) {
            $data = $form['filter_form'];
            $employeeId = isset($data['employeeId']) ? $data['employeeId'] : '';
            $name = isset($data['name']) ? $data['name'] : '';
            $username = isset($data['username']) ? $data['username'] : '';
            $email = isset($data['email']) ? $data['email'] : '';
            $mobile = isset($data['mobile']) ? $data['mobile'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $designation = isset($data['designation']) ? $data['designation'] : '';
            $branch = isset($data['branch']) ? $data['branch'] : '';
            $reportTo = isset($data['reportTo']) ? $data['reportTo'] : '';
            $relieverTo = isset($data['relieverTo']) ? $data['relieverTo'] : '';
            if (!empty($employeeId)) {
                $qb->andWhere($qb->expr()->like("p.employeeId", "'%$employeeId%'"));
            }
            if (!empty($reportTo)) {
                $qb->andWhere('rt.id =:report')->setParameter('report', $reportTo);
            }
            if (!empty($relieverTo)) {
                $qb->andWhere('rl.id =:report')->setParameter('report', $relieverTo);
            }
            if (!empty($branch)) {
                $qb->andWhere('b.id =:branch')->setParameter('branch', $branch);
            }
            if (!empty($department)) {
                $qb->andWhere('dp.id =:department')->setParameter('department', $department);
            }
            if (!empty($designation)) {
                $qb->andWhere('dg.id =:designation')->setParameter('designation', $designation);
            }
            if (!empty($name)) {
                $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
            }
            if (!empty($username)) {
                $qb->andWhere($qb->expr()->like("e.username", "'%$username%'"));
            }
            if (!empty($email)) {
                $qb->andWhere($qb->expr()->like("e.email", "'%$email%'"));
            }
            if (!empty($mobile)) {
                $qb->andWhere($qb->expr()->like("p.mobile", "'%$mobile%'"));
            }
        }

    }

    /**
     * @return User[]
     */
    public function findWithSearchQuery($domain ,$data ): array
    {
        $sort = isset($data['sort']) ? $data['sort'] : 'p.employeeId';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.profile','p');
        $qb->leftJoin('p.designation','dg');
        $qb->leftJoin('p.department','dp');
        $qb->leftJoin('p.branch','b');
        $qb->select('e.id as id','e.name as name','e.email as email','e.username as username','e.enabled as enabled');
        $qb->addSelect('p.mobile as mobile','p.employeeId as employeeId');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('dg.name as designation');
        $qb->addSelect('dp.name as department');
        $qb->where('e.terminal =:domain')->setParameter('domain',$domain);
        $qb->andWhere('e.userGroup =:group')->setParameter('group',"employee");
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}", "$direction");
        $result = $qb;
        return $result;
      //  $qb->orderBy("e.name", "ASC");
       // $result = $qb->getQuery()->getResult();

    }


}
