<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Domain;

use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\Customer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Terminalbd\SecurityBillingBundle\Entity\DeploymentPost;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class BranchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Branch::class);
    }

    public function systemDelete($terminal)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $Branch = $qb->delete(Branch::class, 'e')->where('e.terminal = ?1')->setParameter(1, $terminal)->getQuery();
        if($Branch){
            $Branch->execute();
        }
    }

    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['filter_form'])){
            $data = $form['filter_form'];
            $customer = isset($data['customer'])? $data['customer'] :'';
            $branch = isset($data['branch'])? $data['branch'] :'';
            $district = isset($data['district'])? $data['district'] :'';
            $thana = isset($data['thana'])? $data['thana'] :'';
            $name = isset($data['name'])? $data['name'] :'';
            $mobile = isset($data['mobile'])? $data['mobile'] :'';
            $code = !empty($data['code'])? $data['code'] :'';
            $keyword = isset($data['keyword'])? $data['keyword'] :'';

            if(!empty($customer)){
                $qb->andWhere('c.id =:customer')->setParameter('customer',$customer);
            }
            if(!empty($branch)){
                $qb->andWhere('b.id =:branch')->setParameter('branch',$branch);
            }
            if(!empty($district)){
                $qb->andWhere('p.id =:district')->setParameter('district',$district);
            }
            if(!empty($thana)){
                $qb->andWhere('l.id =:thana')->setParameter('thana',$branch);
            }
            if(!empty($name)){
                $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
            }
             if(!empty($mobile)){
                $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
            }
            if(!empty($code)){
                $qb->andWhere($qb->expr()->like("e.code", "'%$code%'"));
            }
            if (!empty($keyword)) {
                $qb->andWhere('e.name LIKE :searchTerm OR e.code LIKE :searchTerm OR e.mobile LIKE :searchTerm  OR e.address LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$keyword.'%');
            }
        }
    }


    public function getRecords($terminal)
    {

        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.location','l');
        $qb->leftJoin('e.customer','c');
        $qb->select('e.id as id','e.name as name','e.code as code','e.status as status','e.contactPerson as contactMobile','e.contactPerson as contactPerson','e.address as address','e.mobile as mobile','e.email as email');
        $qb->addSelect('l.name as location');
        $qb->addSelect('c.name as customer');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        $qb->andWhere('e.parent IS NULL');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    /**
     * @return Branch[]
     */
    public function findBySearchQuery( $terminal, $data = "" ): array
    {
       // $terminal =  $terminal->getId();
        $sort = isset($data['sort'])? $data['sort'] :'name';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.location','l');
        $qb->leftJoin('l.parent','p');
        $qb->leftJoin('e.customer','c');
        $qb->select('e.id as id','e.name as name','e.code as code','e.status as status','e.billingAddress as billingAddress','e.contactPerson as contactMobile','e.contactPerson as contactPerson','e.address as address','e.mobile as mobile','e.email as email');
        $qb->addSelect('l.name as thana');
        $qb->addSelect('p.name as district');
        $qb->addSelect('c.name as customer');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        $qb->andWhere("e.branchType ='branch'");
        $qb->andWhere("e.isDelete IS NULL")->orWhere("e.isDelete = 0");
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    /**
     * @return Branch[]
     */
    public function findSubBySearchQuery( $terminal, $data = "" ): array
    {

        $sort = isset($data['sort'])? $data['sort'] :'name';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.location','l');
        $qb->leftJoin('l.parent','p');
        $qb->leftJoin('e.parent','b');
        $qb->leftJoin('b.customer','c');
        $qb->select('e.id as id','e.name as name','e.code as code','e.status as status','e.billingAddress as billingAddress','e.contactPerson as contactMobile','e.contactPerson as contactPerson','e.address as address','e.mobile as mobile','e.email as email');
        $qb->addSelect('p.name as district');
        $qb->addSelect('l.name as thana');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('c.name as customer');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        $qb->andWhere("e.branchType ='sub-branch'");
        $qb->andWhere("e.isDelete IS NULL")->orWhere("e.isDelete = 0");
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    /**
     * @return Branch[]
     */
    public function findUnitBySearchQuery( $terminal, $data = "" ): array
    {

        $sort = isset($data['sort'])? $data['sort'] :'name';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.location','l');
        $qb->leftJoin('l.parent','p');
        $qb->leftJoin('e.parent','b');
        $qb->leftJoin('b.customer','c');
        $qb->select('e.id as id','e.name as name','e.code as code','e.status as status','e.billingAddress as billingAddress','e.contactPerson as contactMobile','e.contactPerson as contactPerson','e.address as address','e.mobile as mobile','e.email as email');
        $qb->addSelect('p.name as district');
        $qb->addSelect('l.name as thana');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('c.name as customer');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        $qb->andWhere("e.branchType ='branch-unit'");
        $qb->andWhere("e.isDelete IS NULL")->orWhere("e.isDelete = 0");
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }


    public function getSubRecords($terminal)
    {

        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.location','l');
        $qb->leftJoin('e.parent','parent');
        $qb->leftJoin('e.customer','c');
        $qb->select('e.id as id','e.name as name','e.code as code','e.status as status','e.contactPerson as contactMobile','e.contactPerson as contactPerson','e.address as address','e.mobile as mobile','e.email as email');
        $qb->addSelect('l.name as location');
        $qb->addSelect('c.name as customer');
        $qb->addSelect('parent.name as branch');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        $qb->andWhere('e.parent IS NOT NULL');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function getUpdateBranch($terminal)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.location','l');
        $qb->select('e.id as id','e.name as name','e.code as code','e.status as status','e.contactPerson as contactMobile','e.contactPerson as contactPerson','e.address as address','e.mobile as mobile','e.email as email');
        $qb->addSelect('l.name as location');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        $result = $qb->getQuery()->getArrayResult();
        return $result;


    }

    public function updateBillingAddress(Branch $branch,$address)
    {
        $em = $this->_em;
        $branch->setBillingAddress($address);
        $em->persist($branch);
        $em->flush();
    }


}
