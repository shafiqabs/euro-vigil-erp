<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Domain;

use App\Entity\Admin\AppModule;
use App\Entity\Admin\Terminal;
use App\Entity\Domain\Domain;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class EmailTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailTemplate::class);
    }

    public function insertModule(Terminal $terminal)
    {
        $em = $this->_em;
        $entities = $em->getRepository(AppModule::class)->getSpecificModule($terminal);
        foreach ($entities as $entity){
            $exist = $this->findOneBy(array('terminal' => $terminal,'module' => $entity));
            if(empty($exist)){
                $module = new EmailTemplate();
                $module->setTerminal($terminal->getId());
                $module->setModule($entity);
                $module->setStatus(1);
                $em->persist($module);
                $em->flush();
            }
        }

    }

    public function findEmailTemplate(ModuleProcess $process)
    {
        $terminal = $process->getTerminal();
        $module = $process->getModule()->getId();
        $entity = $this->findOneBy(['terminal' => $terminal,'module' => $module]);
        if($entity){
            return $entity;
        }
        return false;
    }
}
