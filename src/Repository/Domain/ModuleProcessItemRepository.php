<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Domain;

use App\Entity\Admin\AppModule;
use App\Entity\Admin\Bank;
use App\Entity\Domain\BundleRoleGroup;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ModuleProcessItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModuleProcessItem::class);
    }

    public function insertRole(ModuleProcess $process){

        $em = $this->_em;
        $roleGroups = $process->getModule()->getAppBundle()->getBundleRoleGroups();
        $i = 1;
        foreach ($roleGroups as $role){
            $exist = $this->findOneBy(array('moduleProcess' => $process,'bundleRoleGroup'=>$role));
            if(empty($exist)){
                $entity = new ModuleProcessItem();
                $entity->setModuleProcess($process);
                $entity->setBundleRoleGroup($role);
                $entity->setOrdering($i);
                $em->persist($entity);
                $em->flush();
                $i ++;
            }

        }

    }

    public function updateRole($data)
    {
        $em = $this->_em;

        /* @var $entity ModuleProcessItem */

        if(isset($data['processItem']) and sizeof($data['processItem']) > 0){
            foreach ($data['processItem'] as $key => $value){

                $entity = $this->find($value);

                /* if($data['alternativeBundleRoleGroup'][$key]){
                     $alter =$data['alternativeBundleRoleGroup'][$key];
                     $alternativeBundleRoleGroup = $em->getRepository(BundleRoleGroup::class)->find($alter);
                     $entity->setAlternativeBundleRoleGroup($alternativeBundleRoleGroup);
                 }*/
                if(isset($data['isMandatory'][$key]) and $data['isMandatory'][$key] === "on"){
                    $entity->setIsMandatory(true);
                }else{
                    $entity->setIsMandatory(false);
                }
                /* if($data['isRejected'][$key] === "on"){
                     $entity->setIsRejected(true);
                 }
                 if($data['isAcknowledge'][$key] === "on"){
                     $entity->setIsAcknowledge(true);
                 }*/
                /* if($data['status'][$key] === "on"){
                     $entity->setStatus(true);
                 }*/
                if($data['ordering'][$key]){
                    $entity->setOrdering($data['ordering'][$key]);
                }else{
                    $entity->setOrdering($key);
                }
                $em->persist($entity);
                $em->flush();

            }
        }

    }

    /* This function use for  Procurement requisition Process */

    public function checkingOperationalRole(User $user,$process = "")
    {
        $em = $this->_em;
        $roleId =  $user->getUserGroupRole()->getId();
        $module = $em->getRepository(AppModule::class)->findOneBy(array('slug' => $process));
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.moduleProcess','mp');
        $qb->join('e.bundleRoleGroup','brg');
        $qb->where('mp.module =:module')->setParameter("module",$module->getId());
        $qb->andWhere('brg.id =:bundleRoleGroup')->setParameter("bundleRoleGroup",$roleId);
        $currentRole = $qb->getQuery()->getOneOrNullResult();
        if(!empty($currentRole)){
            $moduleProcess = $currentRole->getModuleProcess();
            $ordering = ($currentRole->getOrdering()+1);
            $row = $this->findOneBy(array('moduleProcess' => $moduleProcess,'ordering'=>$ordering));
            if(empty($row)){
                $waitingRole = "";
            }else{
                $waitingRole = $row;
            }
        }

        $array = array('currentRole' => $currentRole ,'waitingRole' => $waitingRole);
        return $array;

    }

    public function labelOrdering($data)
    {

        $i = 1;
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        foreach ($data as $key => $value){
            $val = ($value) ? $value: 0 ;
            $qb->update('SettingAppearanceBundle:MenuGrouping', 'mg')
                ->set('mg.parent', $val)
                ->set('mg.sorting', $i)
                ->where('mg.id = :id')
                ->setParameter('id', $key)
                ->getQuery()
                ->execute();
            $i++;

        }
    }

    public function getMenuTree($arr)
    {
        $value ='';
        $value .='<ol class="dd-list sortable">';
        foreach ($arr as $val) {

            $menu = $val->getMenu()->getMenu();
            if (!empty($menu)) {
                $subIcon = (count($val->getChildren()) > 0 ) ? 1 : 2 ;
                if($subIcon == 1){
                    $value .= '<li style="display:list-item" class="dd-item dd3-item " id="menuItem_'.$val->getId().'">
                     <div class="menuDiv"><span><div data-id="'.$val->getId().'" class="itemTitle dd-handle dd3-handle"></div>
                     <span class="dd3-content">'. $val->getMenu()->getMenu().'</span></span></div>';
                    $value .= $this->getMenuTree($val->getChildren());
                }else{
                    $value .= '<li style="display:list-item" class="dd-item dd3-item " id="menuItem_'.$val->getId().'">
                     <div class="menuDiv"><span><div data-id="'.$val->getId().'" class="itemTitle dd-handle dd3-handle"></div>
                     <span class="dd3-content">' . $val->getMenu()->getMenu().'</span></span></div>';
                }

                $value .= '</li>';
            } else {
                $value .= '<li style="display:list-item" class="dd-item dd3-item " id="menuItem_'.$val->getId().'">
                     <div class="menuDiv"><span><div data-id="'.$val->getId().'" class="itemTitle dd-handle dd3-handle"></div>
                     <span class="dd3-content">'.$val->getMenu()->getMenu() . '</span></span></div>';
            }
        }
        $value .='</ol>';

        return $value;

    }

}
