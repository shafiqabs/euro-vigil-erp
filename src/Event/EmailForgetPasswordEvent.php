<?php
/**
 * Created by PhpStorm.
 * User: dhaka
 * Date: 8/19/14
 * Time: 5:24 PM
 */

namespace App\Event;

use App\Entity\Domain\EmailTemplate;
use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;


class EmailForgetPasswordEvent extends Event
{


    /** @var array */
    protected $emailData;

    public function __construct($emailData)
    {
        $this->emailData = $emailData;
    }



    /**
     * @return array
     */
    public function getEmailData()
    {
        return $this->emailData;
    }


}