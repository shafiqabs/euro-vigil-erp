<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 4/9/15
 * Time: 5:05 PM
 */

namespace App\Event;


use App\Service\EasyMailer;
use Symfony\Contracts\EventDispatcher\Event;

abstract class BaseEmailAwareListener {

    protected $easymailer;

    public function  __construct(EasyMailer $easymailer)
    {
        $this->easymailer = $easymailer;
    }

}