<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Core;

use App\Entity\Core\ItemKeyValue;
use App\Entity\Core\Setting;
use App\Entity\Core\SettingType;
use App\Form\Core\SettingFormType;
use App\Repository\Core\ItemKeyValueRepository;
use App\Repository\Core\SettingRepository;
use App\Repository\Core\SettingTypeRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/core/setting")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SettingController extends AbstractController
{
    /**
     * @Route("/", methods={"GET", "POST"}, name="core_setting")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, SettingRepository $settingRepository, ItemKeyValueRepository $keyValueRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $entity = new Setting();
        $data = $request->request->all();
        $form = $this->createForm(SettingFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setTerminal($terminal);
            $em->persist($entity);
            $em->flush();
           // $keyValueRepository->insertSettingKeyValue($entity,$data);
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('core_setting');
        }
        return $this->render('core/setting/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     * @Route("/new", methods={"GET", "POST"}, name="core_setting_new")
     */
    public function new(Request $request): Response
    {

        $entity = new Setting();
        $data = $request->request->all();

        $form = $this->createForm(SettingFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        return $this->render('core/setting/new.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="core_setting_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function show($id, SettingRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            'core/setting/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="core_setting_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function status($id, SettingRepository $repository): Response
    {
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="core_setting_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function edit(Request $request, $id , SettingRepository $settingRepository, ItemKeyValueRepository $keyValueRepository, TranslatorInterface $translator): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        /* @var $entity Setting */
        $entity = $settingRepository->findOneBy(array('terminal'=>$terminal->getId(),'id'=>$id));
        $form = $this->createForm(SettingFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $keyValueRepository->insertSettingKeyValue($entity,$data);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('core_setting');
        }
        return $this->render('core/setting/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="core_setting_delete")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */

    public function delete($id, SettingRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $entity = $repository->findOneBy(array('terminal'=>$terminal,'id' => $id));
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/meta-delete", methods={"GET"}, name="core_setting_meta_delete")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function metaDelete($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(ItemKeyValue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="core_setting_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */

    public function dataReportingTable(Request $request,SettingRepository $settingRepository)
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $entities = $settingRepository->findBy(array('terminal'=>$terminal));
        $iTotalRecords = $settingRepository->count(array('terminal'=> $terminal));

        $i = 1;
        $records = array();
        $records["data"] = array();
        /* @var $post Setting */

        foreach ($entities as $post):

            $active = empty($post->isStatus()) ? '' : "checked";
            $status ="<input class='status' data-action='{$this->generateUrl('core_setting_status',array('id'=>$post->getId()))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";

            $records["data"][] = array(
                $id                 = $i,
                $setting            = $post->getSettingType()->getName(),
                $name               = $post->getName(),
                $slug               = $post->getSlug(),
                $code               = $post->getCode(),
                $status             = $status,
                $action             ="<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('core_setting_edit',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn btn-mini blue-bg white-font entity-show' data-action='{$this->generateUrl('core_setting_show',array('id'=>$post->getId()))}' href='javascript:' data-title='{$post->getName()}' id='postView-{$post->getId()}' ><i class='feather icon-eye'></i> Show</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post->getId()}' href='javascript:' data-action='{$this->generateUrl('core_setting_delete',array('id'=>$post->getId()))}' id='{$post->getId()}' ><i class='feather icon-trash-2'></i></a>
");
            $i++;
        endforeach;
        return new JsonResponse($records);
    }



}
