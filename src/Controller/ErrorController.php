<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;


use App\Entity\Admin\AppModule;
use App\Entity\Admin\Setting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * @Route("/")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ErrorController extends AbstractController
{
    /**
     * Lists all Post entities.
     * @Route("/", methods={"GET"}, name="error")
     */
    public function index(): Response
    {

        $modules = $this->getDoctrine()->getRepository(AppModule::class)->findBy(array('status'=>1),array('name'=>"ASC"));
        $syndicates = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('slug'=>'business'),array('name'=>"ASC"));

        return $this->render('admin/terminal/index.html.twig', [
            'modules' => $modules,
            'syndicates' => $syndicates,
        ]);
    }

}
