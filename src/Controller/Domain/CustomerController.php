<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Domain;


use App\Entity\Core\Customer;
use App\Entity\Core\Setting;
use App\Entity\Core\Vendor;
use App\Form\Core\VendorFormType;
use App\Service\ConfigureManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller used to manage current user.
 *
 * @Route("/domain/customer")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE_VENDOR')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class CustomerController extends AbstractController
{

    /**
     * Lists all Post entities.
     * @Route("/", methods={"GET"}, name="domain_customer")
     * @Route("/", methods={"GET"}, name="domain_customer_index")
     */
    public function index(): Response
    {
        $businessType = $this->getDoctrine()->getRepository(Setting::class)->getChildRecords("business-type");
        $vendorType = $this->getDoctrine()->getRepository(Setting::class)->getChildRecords("vendor-type");
        return $this->render('domain/vendor/index.html.twig', [
            'businessType' => $businessType,
            'vendorType' => $vendorType,
        ]);

    }


    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE_VENDOR')")
     * @Route("/create", methods={"GET", "POST"}, name="domain_customer_create")
     */
    public function create(Request $request , TranslatorInterface $translator): Response
    {
        $entity = new Vendor();
        $form = $this->createForm(VendorFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setTerminal($this->getUser()->getTerminal()->getId());
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($form->get('mobile')->getData());
            $entity->setMobile($mobile);
            if($form->get('altMobile')->getData()){
                $altMobile = $confManager->specialExpClean($form->get('altMobile')->getData());
                $entity->setAltMobile($altMobile);
            }
            $em->persist($entity);
            $em->flush();
            $msg = $translator->trans('post.insert_successfully');
            return new Response($msg);
        }
        return $this->render('domain/vendor/new.html.twig', [
            'id' => 'postForm',
            'form' => $form->createView(),
            'actionUrl' => $this->generateUrl('domain_customer_create'),
        ]);
    }


    /**
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="domain_customer_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE_VENDOR')")
     */
    public function edit(Request $request, TranslatorInterface $translator ,$id): Response
    {

        $entity = $this->getDoctrine()->getRepository(Vendor::class)->find($id);
        $form = $this->createForm(VendorFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($form->get('mobile')->getData());
            $entity->setMobile($mobile);
            if($form->get('altMobile')->getData()){
                $altMobile = $confManager->specialExpClean($form->get('altMobile')->getData());
                $entity->setAltMobile($altMobile);
            }
            $this->getDoctrine()->getManager()->flush();
            $msg = $translator->trans('post.updated_successfully');
            return new Response($msg);
        }
        return $this->render('domain/vendor/new.html.twig', [
            'actionUrl' => $this->generateUrl('domain_customer_edit',array('id'=> $entity->getId())),
            'post' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Check if mobile available for registering
     * @Route("/{mode}/available", methods={"GET"}, name="domain_customer_process_available")
     * @param   string
     * @return  bool
     */
    function isAvailable(Request $request , $mode) : Response
    {
        $data = $request->query->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $post = $this->getDoctrine()->getRepository(Vendor::class)->checkAvailable($terminal,$mode,$data['vendor_form']);
        return new Response($post);

    }


    /**
     * Check if mobile available for registering
     * @Route("/create-ajax", methods={"POST"}, name="domain_customer_create_ajax")
     * @param   string
     * @return  bool
     */
    function createAjax(Request $request) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $mode = "creatable";
        $status = $this->getDoctrine()->getRepository(Vendor::class)->checkAvailable($terminal,$mode,$data);
        if($status == 'true') {
            $post = new Vendor();
            $post->setTerminal($terminal);
            $vendorType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['vendorType']));
            if ($vendorType) {
                $post->setVendorType($vendorType);
            }
            $post->setName($data['name']);
            $post->setMobile($data['mobile']);
            $businessType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['businessType']));
            if ($businessType) {
                $post->setBusinessType($businessType);
            }
            if($data['vatRegistrationNo']){
                $post->setVatRegistrationNo($data['vatRegistrationNo']);
            }
            if($data['binNo']){
                $post->setBinNo($data['binNo']);
            }
            $this->getDoctrine()->getManager()->persist($post);
            $this->getDoctrine()->getManager()->flush();
            return new Response('valid');
        }else{
            return new Response('invalid');
        }
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="domain_customer_show")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Vendor::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="domain_customer_delete")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Vendor::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="domain_customer_data_table")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE_VENDOR')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $iTotalRecords = $this->getDoctrine()->getRepository(Vendor::class)->count(array('terminal'=> $terminal));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository(Vendor::class)->findBySearchQuery($terminal,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Customer */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('domain_customer_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('domain_customer_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('domain_customer_show',array('id'=> $post['id']));
            $records["data"][] = array(
                $id                 = $i,
                $name               = $post['name'],
                $businessType       = $post['businessType'],
                $vendorType         = $post['vendorType'],
                $company            = $post['companyName'],
                $mobile             = $post['mobile'],
                $registrationNo              = $post['registrationNo'],
                $binNo              = $post['binNo'],
                $vatRegistrationNo              = $post['vatRegistrationNo'],
                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
 <li class='dropdown-item'> <a data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' >View</a></li>
<li class='dropdown-item'> <a data-action='{$editUrl}' href='?process=postEdit-{$post['id']}&check=edit#modal' id='postEdit-{$post['id']}'>Edit</a></li>
<li class='dropdown-item'> <a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }
}
