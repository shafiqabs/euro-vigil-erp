<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Domain;


use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Form\Domain\ModuleProcessFormType;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Repository\Domain\ModuleProcessRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/domain/module-process")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ModuleProcessController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="domain_moduleprocess")
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, ModuleProcessRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $repository->insertModule($terminal);
        $entities = $this->getDoctrine()->getRepository(ModuleProcess::class)->findBy(['terminal'=>$terminal]);
        return $this->render('domain/moduleProcess/index.html.twig',['entities' => $entities]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN')")
     * @Route("/new", methods={"GET", "POST"}, name="domain_moduleprocess_new")
     */
    public function new(Request $request, TranslatorInterface $translator,ModuleProcessRepository $repository, ModuleProcessItemRepository $processItemRepository): Response
    {

        $entity = new ModuleProcess();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $form = $this->createForm(ModuleProcessFormType::class, $entity, array('terminal' => $terminal))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $exist = $repository->findOneBy(array('terminal'=>$terminal->getId(),'module'=>$entity->getModule()->getId()));
            if(empty($exist)){
                $em = $this->getDoctrine()->getManager();
                $entity->setTerminal($this->getUser()->getTerminal()->getId());
                $em->persist($entity);
                $em->flush();
                $processItemRepository->insertRole($entity);
                $message = $translator->trans('data.created_successfully');
                $this->addFlash('success', $message);
                if ($form->get('SaveAndCreate')->isClicked()) {
                    return $this->redirectToRoute('domain_moduleprocess_edit',array('id'=>$entity->getId()));
                }
            }else{
                $message = $translator->trans('data.created_successfully');
                $this->addFlash('notice', "This module is already created");
                return $this->redirectToRoute('domain_moduleprocess_edit',array('id'=>$exist->getId()));
            }
            return $this->redirectToRoute('domain_moduleprocess');
        }
        return $this->render('domain/moduleProcess/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="domain_moduleprocess_edit")
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, ModuleProcess $entity, TranslatorInterface $translator, ModuleProcessRepository $repository, ModuleProcessItemRepository $processItemRepository): Response
    {
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $form = $this->createForm(ModuleProcessFormType::class, $entity, array('terminal' => $terminal))
            ->add('SaveAndCreate', SubmitType::class);
        $processItemRepository->insertRole($entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $processItemRepository->updateRole($data);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message); $this->addFlash('success', 'post.updated_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('domain_moduleprocess_edit', ['id' => $entity->getId()]);
            }
            return $this->redirectToRoute('domain_moduleprocess');
        }
        return $this->render('domain/moduleProcess/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a moduleProcess entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="domain_moduleprocess_delete")
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function delete($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(ModuleProcess::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/{process}/status", methods={"GET"}, name="domain_moduleprocess_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id,$process, ModuleProcessRepository $repository): Response
    {
        /* @var $entity ModuleProcess */

        $entity = $repository->find($id);
        if($process == "status"){
            $status = $_REQUEST['status'];
            if($status == "false"){
                $entity->setStatus(false);
            }else{
                $entity->setStatus(true);
            }
        }elseif($process == "attachment"){
            $status = $_REQUEST['status'];
            if($status == "false"){
                $entity->setAttachment(false);
            }else{
                $entity->setAttachment(true);
            }
        }elseif($process == "approverForm"){
            $status = $_REQUEST['status'];
            if($status == "false"){
                $entity->setApproverForm(false);
            }else{
                $entity->setApproverForm(true);
            }
        }elseif($process == "approverReject"){
            $status = $_REQUEST['status'];
            if($status == "false"){
                $entity->setApproverReject(false);
            }else{
                $entity->setApproverReject(true);
            }
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/item-status", methods={"GET"}, name="domain_moduleprocess_item_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function itemstatus($id,ModuleProcessItemRepository $repository): Response
    {
        /* @var $entity ModuleProcessItem */

        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     *  moduleProcessItem entity.
     *
     * @Route("/module-process-item", methods={"GET"}, name="domain_moduleprocess_sorting", options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */

    public function moduleItemOrdering(ModuleProcessItemRepository $repository): Response
    {

        $data = $_REQUEST;
        $id = $data['columnId'];
        $sorting = $data['sorting'];
        $entity = $repository->find($id);
        $entity->setOrdering($sorting);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }



}
