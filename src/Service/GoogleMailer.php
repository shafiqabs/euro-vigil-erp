<?php

namespace App\Service;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class GoogleMailer
{

    public function sendMail(MailerInterface $mailer, $data)
    {

        $from = $data['from'];
        $to = $data['to'];
        $subject = $data['subject'];
        $body = $data['body'];
        $email = (new Email())
            ->from($from)
            ->to($to)
            ->subject($subject)
            ->text($body);
        if($mailer->send($email)){
            echo "Success";
        }
        echo "Failed!";
    }

} 