<?php

namespace App\Service;


use Symfony\Bundle\MonologBundle\SwiftMailer\MessageFactory;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EasyMailer
{

    public function mailer()
    {
        $username = $_ENV['MAIL_USERNAME'];
        $password = $_ENV['MAIL_PASSWORD'];
        $mailHost = $_ENV['MAIL_HOST'];
        $mailPort = $_ENV['MAIL_PORT'];
        $mailEncryption = $_ENV['MAIL_ENCRYPTION'];
        $transport = (new \Swift_SmtpTransport($mailHost, $mailPort,$mailEncryption))
            ->setUsername($username)
            ->setPassword($password);
        $mailer = new \Swift_Mailer($transport);
        return $mailer;
    }


} 