<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\EventSubscriber;

use App\Entity\Domain\EmailTemplate;
use App\Event\EmailEvent;
use App\Service\EasyMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Notifies post's author about new comments.
 *
 * @author Oleg Voronkovich <oleg-voronkovich@yandex.ru>
 */
class ApproveNotificationSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $translator;
    private $urlGenerator;
    private $sender;
    private $senderName;
    /** @var EasyMailer */
    protected $easymailer;

    public function __construct(MailerInterface $mailer, UrlGeneratorInterface $urlGenerator, TranslatorInterface $translator,EasyMailer $easymailer, $sender,$senderName)
    {
        $this->mailer = $mailer;
        $this->urlGenerator = $urlGenerator;
        $this->translator = $translator;
        $this->sender = $sender;
        $this->senderName = $senderName;
        $this->easymailer = $easymailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            EmailEvent::class => 'onCommentCreated',
        ];
    }

    public function onCommentCreated(EmailEvent $event): void
    {
        /** @var EmailTemplate $template */

        $template           = $event->getEmailTemplate();
        $emailData          = $event->getEmailData();

        $templateTitle      = $template->getName();
        $templateBody       = $template->getBody();


        $link               = $emailData['link'];
        $invoice            = $emailData['invoice'];
        $created            = $emailData['created'];
        $toName             = $emailData['toName'];
        $toEmail            = $emailData['toEmail'];

        $search = array('[%created%]','[%link%]','[%invoice-no%]');
        $replace = array($created , $link , $invoice);
        $body =  str_ireplace($search, $replace, $templateBody);
        /*$mailer =  $this->easymailer->mailer();
        $message = (new \Swift_Message($templateTitle))
            ->setFrom([ $this->sender => $this->senderName])
            ->setTo(['imammahadi.ovi@gmail.com' => 'Imam Mahadi','shk.farukahmed@gmail.com' => 'Sheik Faruk Ahmed','leon@amanknittings.com' => 'Leon','terminalbd@gmail.com' => 'Md Shafiqul Islam'])
            ->setBody($body,'text/html');
        $mailer->send($message);*/
    }
}
