<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Domain;
use App\Entity\Core\Setting;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Domain\ModuleProcessItemRepository")
 * @ORM\Table(name="dom_module_process_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ModuleProcessItem
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess", inversedBy="moduleProcessItems", cascade="remove")
     */
    private $moduleProcess;

    /**
     * @var BundleRoleGroup
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\BundleRoleGroup")
     */
    private $bundleRoleGroup;


    /**
     * @var BundleRoleGroup
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\BundleRoleGroup")
     */
    private $alternativeBundleRoleGroup;


    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    private $switchDepartment;


    /**
     * @var int
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $ordering;


    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isMandatory= false;


    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isRejected= false;


    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isAcknowledge = false;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return BundleRoleGroup
     */
    public function getBundleRoleGroup()
    {
        return $this->bundleRoleGroup;
    }

    /**
     * @param BundleRoleGroup $bundleRoleGroup
     */
    public function setBundleRoleGroup($bundleRoleGroup)
    {
        $this->bundleRoleGroup = $bundleRoleGroup;
    }

    /**
     * @return BundleRoleGroup
     */
    public function getAlternativeBundleRoleGroup()
    {
        return $this->alternativeBundleRoleGroup;
    }

    /**
     * @param BundleRoleGroup $alternativeBundleRoleGroup
     */
    public function setAlternativeBundleRoleGroup($alternativeBundleRoleGroup)
    {
        $this->alternativeBundleRoleGroup = $alternativeBundleRoleGroup;
    }

    /**
     * @return Setting
     */
    public function getSwitchDepartment()
    {
        return $this->switchDepartment;
    }

    /**
     * @param Setting $switchDepartment
     */
    public function setSwitchDepartment($switchDepartment)
    {
        $this->switchDepartment = $switchDepartment;
    }

    /**
     * @return int
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering)
    {
        $this->ordering = $ordering;
    }

    /**
     * @return bool
     */
    public function isMandatory()
    {
        return $this->isMandatory;
    }

    /**
     * @param bool $isMandatory
     */
    public function setIsMandatory($isMandatory)
    {
        $this->isMandatory = $isMandatory;
    }

    /**
     * @return bool
     */
    public function isRejected()
    {
        return $this->isRejected;
    }

    /**
     * @param bool $isRejected
     */
    public function setIsRejected($isRejected)
    {
        $this->isRejected = $isRejected;
    }

    /**
     * @return bool
     */
    public function isAcknowledge()
    {
        return $this->isAcknowledge;
    }

    /**
     * @param bool $isAcknowledge
     */
    public function setIsAcknowledge($isAcknowledge)
    {
        $this->isAcknowledge = $isAcknowledge;
    }

    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess($moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }

}
