<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Domain;
use App\Entity\Admin\AppBundle;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Domain\BundleRoleGroupRepository")
 * @ORM\Table(name="domain_bundle_role_group")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BundleRoleGroup
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="terminal", type="integer", nullable=true)
     */
    private $terminal;

    /**
     * @var AppBundle
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\AppBundle", inversedBy="bundleRoleGroups")
     */
    private $appBundle;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;


     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $roleName;

    /**
     * @var array
     * @ORM\Column(type="json", nullable=true)
     */
    private $roles = [];

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $process;


    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $roleGroup;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return AppBundle
     */
    public function getAppBundle()
    {
        return $this->appBundle;
    }

    /**
     * @param AppBundle $appBundle
     */
    public function setAppBundle(AppBundle $appBundle)
    {
        $this->appBundle = $appBundle;
    }

    /**
     * @return int
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param int $terminal
     */
    public function setTerminal(int $terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * @param string $roleName
     */
    public function setRoleName($roleName)
    {
        $this->roleName = $roleName;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantees that a user always has at least one role for security
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }
        return array_unique($roles);
    }

    /**
     * @param array $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return string
     */
    public function getRoleGroup()
    {
        return $this->roleGroup;
    }

    /**
     * @param string $roleGroup
     */
    public function setRoleGroup($roleGroup)
    {
        $this->roleGroup = $roleGroup;
    }


}
