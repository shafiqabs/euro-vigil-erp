<?php

namespace App\Entity\Domain;

use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Domain
 * @ORM\Table("domain_email_template")
 * @UniqueEntity(fields={"module","terminal"}, message="This module name must be unique")
 * @ORM\Entity(repositoryClass="App\Repository\Domain\EmailTemplateRepository")
 */
class EmailTemplate
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\AppModule")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected $module;


    /**
     * @var integer
     * @ORM\Column(type="integer",  nullable=true )
     */
    private $terminal;

    /**
     * @var string
     * @ORM\Column(type="string", length=255  , nullable=true )
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text",  nullable=true )
     */
    private $body;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param mixed $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return int
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param int $terminal
     */
    public function setTerminal(int $terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}
