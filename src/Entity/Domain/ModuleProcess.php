<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Domain;
use App\Entity\Admin\AppModule;
use App\Entity\Admin\Terminal;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Domain\ModuleProcessRepository")
 * @ORM\Table(name="dom_module_process")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ModuleProcess
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $terminal;


    /**
     * * @var AppModule
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\AppModule")
     **/
    private $module;


    /**
     * * @var ModuleProcessItem
     * @ORM\OneToMany(targetEntity="App\Entity\Domain\ModuleProcessItem",mappedBy="moduleProcess")
     * @ORM\OrderBy({"ordering" = "ASC"})
     **/
    private $moduleProcessItems;


    /**
     * @var AppModule
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\AppModule")
     * @ORM\JoinColumn(name="end_module_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     **/
    private $endModule;


     /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $endStatus;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $attachment;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $approverForm;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $approverReject;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $approveType = "user";

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $operationGroup;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return AppModule
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param AppModule $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return int
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param int $terminal
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
    }


    /**
     * @return string
     */
    public function getEndStatus()
    {
        return $this->endStatus;
    }

    /**
     * @param string $endStatus
     */
    public function setEndStatus( $endStatus)
    {
        $this->endStatus = $endStatus;
    }

    /**
     * @return AppModule
     */
    public function getEndModule()
    {
        return $this->endModule;
    }

    /**
     * @param AppModule $endModule
     */
    public function setEndModule($endModule)
    {
        $this->endModule = $endModule;
    }

    /**
     * @return ModuleProcessItem
     */
    public function getModuleProcessItems()
    {
        return $this->moduleProcessItems;
    }

    /**
     * @return bool
     */
    public function isAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param bool $attachment
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * @return string
     */
    public function getApproveType()
    {
        return $this->approveType;
    }

    /**
     * @param string $approveType
     */
    public function setApproveType( $approveType)
    {
        $this->approveType = $approveType;
    }

    /**
     * @return string
     */
    public function getOperationGroup()
    {
        return $this->operationGroup;
    }

    /**
     * @param string $operationGroup
     */
    public function setOperationGroup($operationGroup)
    {
        $this->operationGroup = $operationGroup;
    }

    /**
     * @return bool
     */
    public function isApproverForm()
    {
        return $this->approverForm;
    }

    /**
     * @param bool $approverForm
     */
    public function setApproverForm(bool $approverForm)
    {
        $this->approverForm = $approverForm;
    }

    /**
     * @return bool
     */
    public function isApproverReject()
    {
        return $this->approverReject;
    }

    /**
     * @param bool $approverReject
     */
    public function setApproverReject(bool $approverReject)
    {
        $this->approverReject = $approverReject;
    }


}
