<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Core;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\UserReportingRepository")
 * @ORM\Table(name="core_user_reporting")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class UserReporting
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="userReporting")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;


    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userReportingTo")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $reportTo;


     /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userRelieverTo")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $relieverTo;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param User $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }


    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return User
     */
    public function getRelieverTo()
    {
        return $this->relieverTo;
    }

    /**
     * @param User $relieverTo
     */
    public function setRelieverTo($relieverTo)
    {
        $this->relieverTo = $relieverTo;
    }


}
