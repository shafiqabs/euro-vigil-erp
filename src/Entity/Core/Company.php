<?php

namespace App\Entity\Core;

use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * GlobalOption
 * @ORM\Table("core_company")
 * @ORM\Entity(repositoryClass="App\Repository\Core\CompanyRepository")
 */
class Company
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Admin\Terminal", inversedBy="company")
     **/
    protected $terminal;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    protected $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Location")
     **/
    protected $location;


    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $companyName;

    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $registrationCategory;

     /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable = true )
     */
    private $holdingEntity = false;

    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $tradeLicense;

    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $tradeLicenseIssueDate;

    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $incorporationNo;

    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $incorporationIssueDate;

    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $eTin;

    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $eTinNameOfCompany;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $differentEtin;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $tradingBrandName;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $registrationType;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $equityInformation;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $equityLocalShare;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $bidaRegistrationNo;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $bidaIssueDate;


    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $companyOperationAddress;


     /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $district;


     /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $policeStation;


    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $postCode;


    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $landPhoneNo;


     /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $mobile;


    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $email;


    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $faxNumber;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $webAddress;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $headQuarterAddress;


    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $headQuarterAddressOutside;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $ircNumber;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $ercNumber;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $businessOtherSpecify;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $ircIssueDate;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $ercIssueDate;

    /**
     * @var float
     * @ORM\Column(type="float", nullable = true )
     */
    private $taxableTurnover;

    /**
     * @var float
     * @ORM\Column(type="float", nullable = true )
     */
    private $projectedTurnover;


    /**
     * @var float
     * @ORM\Column(type="float", nullable = true )
     */
    private $noOfEmployee;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable = true )
     */
    private $zeroRatedSupply;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable = true )
     */
    private $vatExemptedSupply;


    /**
     * @var string
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;


    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=true)
     */
    private $status = 0;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isManufacturing = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isServicing = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isExport = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isImport = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEconomicActivityOther = 0;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $economicActivityOther;


    /**
     * @var integer
     *
     * @ORM\Column(name="uniqueKey", type="integer", nullable=true)
     */
    private $uniqueKey;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }



    /**
     * @return string
     */
    public function getAddress(): ? string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }


    /**
     * @return Setting
     */
    public function getVatSlave()
    {
        return $this->vatSlave;
    }

    /**
     * @param Setting $vatSlave
     */
    public function setVatSlave($vatSlave)
    {
        $this->vatSlave = $vatSlave;
    }

    /**
     * @return Setting
     */
    public function getBusinessType()
    {
        return $this->businessType;
    }

    /**
     * @param Setting $businessType
     */
    public function setBusinessType($businessType)
    {
        $this->businessType = $businessType;
    }


    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return \App\Entity\Core\Setting
     */
    public function getBusinessOwnership()
    {
        return $this->businessOwnership;
    }

    /**
     * @param \App\Entity\Core\Setting $businessOwnership
     */
    public function setBusinessOwnership($businessOwnership)
    {
        $this->businessOwnership = $businessOwnership;
    }

    /**
     * @return \App\Entity\Core\Setting
     */
    public function getEconomicActivity()
    {
        return $this->economicActivity;
    }

    /**
     * @param \App\Entity\Core\Setting $economicActivity
     */
    public function setEconomicActivity($economicActivity)
    {
        $this->economicActivity = $economicActivity;
    }

    /**
     * @return \App\Entity\Core\Setting
     */
    public function getManufacturingArea()
    {
        return $this->manufacturingArea;
    }

    /**
     * @param \App\Entity\Core\Setting $manufacturingArea
     */
    public function setManufacturingArea($manufacturingArea)
    {
        $this->manufacturingArea = $manufacturingArea;
    }

    /**
     * @return \App\Entity\Core\Setting
     */
    public function getServiceArea()
    {
        return $this->serviceArea;
    }

    /**
     * @param \App\Entity\Core\Setting $serviceArea
     */
    public function setServiceArea($serviceArea)
    {
        $this->serviceArea = $serviceArea;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName(string $companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getRegistrationCategory()
    {
        return $this->registrationCategory;
    }

    /**
     * @param string $registrationCategory
     */
    public function setRegistrationCategory(string $registrationCategory)
    {
        $this->registrationCategory = $registrationCategory;
    }

    /**
     * @return Terminal
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param Terminal $terminal
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return string
     */
    public function getOldBin()
    {
        return $this->oldBin;
    }

    /**
     * @param string $oldBin
     */
    public function setOldBin(string $oldBin)
    {
        $this->oldBin = $oldBin;
    }

    /**
     * @return string
     */
    public function getOtherOwnershipType()
    {
        return $this->otherOwnershipType;
    }

    /**
     * @param string $otherOwnershipType
     */
    public function setOtherOwnershipType(string $otherOwnershipType)
    {
        $this->otherOwnershipType = $otherOwnershipType;
    }

    /**
     * @return bool
     */
    public function isHoldingEntity()
    {
        return $this->holdingEntity;
    }

    /**
     * @param bool $holdingEntity
     */
    public function setHoldingEntity(bool $holdingEntity)
    {
        $this->holdingEntity = $holdingEntity;
    }

    /**
     * @return string
     */
    public function getTradeLicense()
    {
        return $this->tradeLicense;
    }

    /**
     * @param string $tradeLicense
     */
    public function setTradeLicense(string $tradeLicense)
    {
        $this->tradeLicense = $tradeLicense;
    }

    /**
     * @return string
     */
    public function getIncorporationNo()
    {
        return $this->incorporationNo;
    }

    /**
     * @param string $incorporationNo
     */
    public function setIncorporationNo(string $incorporationNo)
    {
        $this->incorporationNo = $incorporationNo;
    }

    /**
     * @return string
     */
    public function getETin()
    {
        return $this->eTin;
    }

    /**
     * @param string $eTin
     */
    public function setETin(string $eTin)
    {
        $this->eTin = $eTin;
    }

    /**
     * @return string
     */
    public function getDifferentEtin()
    {
        return $this->differentEtin;
    }

    /**
     * @param string $differentEtin
     */
    public function setDifferentEtin(string $differentEtin)
    {
        $this->differentEtin = $differentEtin;
    }

    /**
     * @return string
     */
    public function getTradingBrandName()
    {
        return $this->tradingBrandName;
    }

    /**
     * @param string $tradingBrandName
     */
    public function setTradingBrandName(string $tradingBrandName)
    {
        $this->tradingBrandName = $tradingBrandName;
    }

    /**
     * @return string
     */
    public function getRegistrationType()
    {
        return $this->registrationType;
    }

    /**
     * @param string $registrationType
     */
    public function setRegistrationType(string $registrationType)
    {
        $this->registrationType = $registrationType;
    }

    /**
     * @return string
     */
    public function getEquityInformation()
    {
        return $this->equityInformation;
    }

    /**
     * @param string $equityInformation
     * 100% Foreign
     * Joint Venture
     * Local Share
     */

    public function setEquityInformation(string $equityInformation)
    {
        $this->equityInformation = $equityInformation;
    }

    /**
     * @return string
     */
    public function getBidaRegistrationNo()
    {
        return $this->bidaRegistrationNo;
    }

    /**
     * @param string $bidaRegistrationNo
     */
    public function setBidaRegistrationNo(string $bidaRegistrationNo)
    {
        $this->bidaRegistrationNo = $bidaRegistrationNo;
    }

    /**
     * @return string
     */
    public function getBidaIssueDate()
    {
        return $this->bidaIssueDate;
    }

    /**
     * @param string $bidaIssueDate
     */
    public function setBidaIssueDate(string $bidaIssueDate)
    {
        $this->bidaIssueDate = $bidaIssueDate;
    }

    /**
     * @return string
     */
    public function getCompanyOperationAddress()
    {
        return $this->companyOperationAddress;
    }

    /**
     * @param string $companyOperationAddress
     */
    public function setCompanyOperationAddress(string $companyOperationAddress)
    {
        $this->companyOperationAddress = $companyOperationAddress;
    }

    /**
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param string $postCode
     */
    public function setPostCode(string $postCode)
    {
        $this->postCode = $postCode;
    }

    /**
     * @return string
     */
    public function getLandPhoneNo()
    {
        return $this->landPhoneNo;
    }

    /**
     * @param string $landPhoneNo
     */
    public function setLandPhoneNo(string $landPhoneNo)
    {
        $this->landPhoneNo = $landPhoneNo;
    }

    /**
     * @return string
     */
    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    /**
     * @param string $faxNumber
     */
    public function setFaxNumber(string $faxNumber)
    {
        $this->faxNumber = $faxNumber;
    }

    /**
     * @return string
     */
    public function getWebAddress()
    {
        return $this->webAddress;
    }

    /**
     * @param string $webAddress
     */
    public function setWebAddress(string $webAddress)
    {
        $this->webAddress = $webAddress;
    }

    /**
     * @return string
     */
    public function getHeadQuarterAddress()
    {
        return $this->headQuarterAddress;
    }

    /**
     * @param string $headQuarterAddress
     */
    public function setHeadQuarterAddress(string $headQuarterAddress)
    {
        $this->headQuarterAddress = $headQuarterAddress;
    }

    /**
     * @return string
     */
    public function getHeadQuarterAddressOutside()
    {
        return $this->headQuarterAddressOutside;
    }

    /**
     * @param string $headQuarterAddressOutside
     */
    public function setHeadQuarterAddressOutside(string $headQuarterAddressOutside)
    {
        $this->headQuarterAddressOutside = $headQuarterAddressOutside;
    }

    /**
     * @return string
     */
    public function getIrcNumber()
    {
        return $this->ircNumber;
    }

    /**
     * @param string $ircNumber
     */
    public function setIrcNumber(string $ircNumber)
    {
        $this->ircNumber = $ircNumber;
    }

    /**
     * @return string
     */
    public function getErcNumber()
    {
        return $this->ercNumber;
    }

    /**
     * @param string $ercNumber
     */
    public function setErcNumber(string $ercNumber)
    {
        $this->ercNumber = $ercNumber;
    }

    /**
     * @return string
     */
    public function getBusinessOtherSpecify()
    {
        return $this->businessOtherSpecify;
    }

    /**
     * @param string $businessOtherSpecify
     */
    public function setBusinessOtherSpecify(string $businessOtherSpecify)
    {
        $this->businessOtherSpecify = $businessOtherSpecify;
    }

    /**
     * @return string
     */
    public function getIrcIssueDate()
    {
        return $this->ircIssueDate;
    }

    /**
     * @param string $ircIssueDate
     */
    public function setIrcIssueDate(string $ircIssueDate)
    {
        $this->ircIssueDate = $ircIssueDate;
    }

    /**
     * @return string
     */
    public function getErcIssueDate()
    {
        return $this->ercIssueDate;
    }

    /**
     * @param string $ercIssueDate
     */
    public function setErcIssueDate(string $ercIssueDate)
    {
        $this->ercIssueDate = $ercIssueDate;
    }

    /**
     * @return float
     */
    public function getTaxableTurnover()
    {
        return $this->taxableTurnover;
    }

    /**
     * @param float $taxableTurnover
     */
    public function setTaxableTurnover(float $taxableTurnover)
    {
        $this->taxableTurnover = $taxableTurnover;
    }

    /**
     * @return float
     */
    public function getProjectedTurnover()
    {
        return $this->projectedTurnover;
    }

    /**
     * @param float $projectedTurnover
     */
    public function setProjectedTurnover(float $projectedTurnover)
    {
        $this->projectedTurnover = $projectedTurnover;
    }

    /**
     * @return float
     */
    public function getNoOfEmployee()
    {
        return $this->noOfEmployee;
    }

    /**
     * @param float $noOfEmployee
     */
    public function setNoOfEmployee(float $noOfEmployee)
    {
        $this->noOfEmployee = $noOfEmployee;
    }

    /**
     * @return bool
     */
    public function isZeroRatedSupply()
    {
        return $this->zeroRatedSupply;
    }

    /**
     * @param bool $zeroRatedSupply
     */
    public function setZeroRatedSupply(bool $zeroRatedSupply)
    {
        $this->zeroRatedSupply = $zeroRatedSupply;
    }

    /**
     * @return bool
     */
    public function isVatExemptedSupply()
    {
        return $this->vatExemptedSupply;
    }

    /**
     * @param bool $vatExemptedSupply
     */
    public function setVatExemptedSupply(bool $vatExemptedSupply)
    {
        $this->vatExemptedSupply = $vatExemptedSupply;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getUniqueKey(): int
    {
        return $this->uniqueKey;
    }

    /**
     * @param int $uniqueKey
     */
    public function setUniqueKey(int $uniqueKey)
    {
        $this->uniqueKey = $uniqueKey;
    }

    /**
     * @return string
     */
    public function getTradeLicenseIssueDate()
    {
        return $this->tradeLicenseIssueDate;
    }

    /**
     * @param string $tradeLicenseIssueDate
     */
    public function setTradeLicenseIssueDate(string $tradeLicenseIssueDate)
    {
        $this->tradeLicenseIssueDate = $tradeLicenseIssueDate;
    }

    /**
     * @return string
     */
    public function getIncorporationIssueDate()
    {
        return $this->incorporationIssueDate;
    }

    /**
     * @param string $incorporationIssueDate
     */
    public function setIncorporationIssueDate(string $incorporationIssueDate)
    {
        $this->incorporationIssueDate = $incorporationIssueDate;
    }

    /**
     * @return string
     */
    public function getETinNameOfCompany()
    {
        return $this->eTinNameOfCompany;
    }

    /**
     * @param string $eTinNameOfCompany
     */
    public function setETinNameOfCompany(string $eTinNameOfCompany)
    {
        $this->eTinNameOfCompany = $eTinNameOfCompany;
    }

    /**
     * @return string
     */
    public function getEquityLocalShare()
    {
        return $this->equityLocalShare;
    }

    /**
     * @param string $equityLocalShare
     */
    public function setEquityLocalShare(string $equityLocalShare)
    {
        $this->equityLocalShare = $equityLocalShare;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param string $district
     */
    public function setDistrict(string $district)
    {
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getPoliceStation()
    {
        return $this->policeStation;
    }

    /**
     * @param string $policeStation
     */
    public function setPoliceStation(string $policeStation)
    {
        $this->policeStation = $policeStation;
    }

    /**
     * @return bool
     */
    public function isManufacturing()
    {
        return $this->isManufacturing;
    }

    /**
     * @param bool $isManufacturing
     */
    public function setIsManufacturing(bool $isManufacturing)
    {
        $this->isManufacturing = $isManufacturing;
    }

    /**
     * @return bool
     */
    public function isServicing()
    {
        return $this->isServicing;
    }

    /**
     * @param bool $isServicing
     */
    public function setIsServicing(bool $isServicing)
    {
        $this->isServicing = $isServicing;
    }

    /**
     * @return bool
     */
    public function isExport()
    {
        return $this->isExport;
    }

    /**
     * @param bool $isExport
     */
    public function setIsExport(bool $isExport)
    {
        $this->isExport = $isExport;
    }

    /**
     * @return bool
     */
    public function isImport()
    {
        return $this->isImport;
    }

    /**
     * @param bool $isImport
     */
    public function setIsImport(bool $isImport)
    {
        $this->isImport = $isImport;
    }

    /**
     * @return bool
     */
    public function isEconomicActivityOther()
    {
        return $this->isEconomicActivityOther;
    }

    /**
     * @param bool $isEconomicActivityOther
     */
    public function setIsEconomicActivityOther(bool $isEconomicActivityOther)
    {
        $this->isEconomicActivityOther = $isEconomicActivityOther;
    }

    /**
     * @return string
     */
    public function getEconomicActivityOther()
    {
        return $this->economicActivityOther;
    }

    /**
     * @param string $economicActivityOther
     */
    public function setEconomicActivityOther(string $economicActivityOther)
    {
        $this->economicActivityOther = $economicActivityOther;
    }

    /**
     * @return string
     */
    public function getManufacturingOther()
    {
        return $this->manufacturingOther;
    }

    /**
     * @param string $manufacturingOther
     */
    public function setManufacturingOther(string $manufacturingOther)
    {
        $this->manufacturingOther = $manufacturingOther;
    }

    /**
     * @return Setting
     */
    public function getServicingArea()
    {
        return $this->servicingArea;
    }

    /**
     * @param Setting $servicingArea
     */
    public function setServicingArea(Setting $servicingArea)
    {
        $this->servicingArea = $servicingArea;
    }

    /**
     * @return string
     */
    public function getServicingOther()
    {
        return $this->servicingOther;
    }

    /**
     * @param string $servicingOther
     */
    public function setServicingOther(string $servicingOther)
    {
        $this->servicingOther = $servicingOther;
    }


}
