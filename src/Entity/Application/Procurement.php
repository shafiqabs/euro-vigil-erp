<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Application;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\NbrvatBundle\Entity\TaxSetup;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Application\ProcurementRepository")
 * @ORM\Table(name="tbd_procurement")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Procurement
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $terminal;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status = 0;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $directTenderAmount = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $boardApproveAmount = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $csMode = 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return bool
     */
    public function isStatus(): ? bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param int $terminal
     */
    public function setTerminal(int $terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return float
     */
    public function getDirectTenderAmount()
    {
        return $this->directTenderAmount;
    }

    /**
     * @param float $directTenderAmount
     */
    public function setDirectTenderAmount($directTenderAmount)
    {
        $this->directTenderAmount = $directTenderAmount;
    }

    /**
     * @return string
     */
    public function getCsMode()
    {
        return $this->csMode;
    }

    /**
     * @param string $csMode
     * PR Item Base
     * Stock Book Item
     * Stock Item
     */
    public function setCsMode($csMode)
    {
        $this->csMode = $csMode;
    }

    /**
     * @return float
     */
    public function getBoardApproveAmount()
    {
        return $this->boardApproveAmount;
    }

    /**
     * @param float $boardApproveAmount
     */
    public function setBoardApproveAmount($boardApproveAmount)
    {
        $this->boardApproveAmount = $boardApproveAmount;
    }


}
