<?php

namespace App\Entity\Admin;


use App\Entity\Domain\Domain;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Terminal
 * @UniqueEntity(fields="mobile",message="This mobile is already in use.")
 * @ORM\Table("tbd")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\TerminalRepository")
 */
class Terminal
{

    const PROCESS_ACTIVE = 'Active';
    const PROCESS_INACTIVE = 'In-active';
    const PROCESS_SUSPEND = 'Suspend';

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\AppBundle", inversedBy="appDomains" , cascade={"detach","merge"} )
     **/
    protected $mainApp;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Domain\Domain", mappedBy="terminal")
     **/
    protected $coreDomain;

    /**
     * @ORM\ManyToOne(targetEntity="Location", inversedBy="terminals" , cascade={"detach","merge"} )
     **/
    protected $location;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Setting")
     **/
    protected $businessGroup;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="terminal")
     **/
    protected $users;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Admin\AppBundle", inversedBy="terminals")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected $appBundles;


    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="mobile", type="string", length=50, unique = true, nullable = true )
     */
    private $mobile;

    /**
     * @var string
     * @ORM\Column(name="displayMobile", type="string", length=50, nullable = true )
     */
    private $displayMobile;


    /**
     * @var string
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=100, nullable = true )
     */
    private $email;


    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255  , nullable=true )
     */
    private $name;


    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(unique=true, nullable=true)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(name="domain", type="string", length=255 ,nullable=true)
     */
    private $domain;

    /**
     * @var string
     * @ORM\Column(type="string", length=100 ,nullable=true)
     */
    private $uniqueKey;


    /**
     * @var string
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status = true;


     /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $approveProcess = true;


    /**
     * @var string $type
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $process;


     /**
     * @var string $type
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $binNo;

    /**
     * @var string $type
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tinNo;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    function __construct()
    {
        $this->created = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set domain
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }



    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }



    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getDisplayMobile()
    {
        return $this->displayMobile;
    }

    /**
     * @param string $displayMobile
     */
    public function setDisplayMobile($displayMobile)
    {
        $this->displayMobile = $displayMobile;
    }


    /**
     * @return User
     */
    public function getUsers()
    {
        return $this->users;
    }


    /**
     * @param Location $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }


    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return AppModule
     */
    public function getMainApp()
    {
        return $this->mainApp;
    }

    /**
     * @param AppModule $mainApp
     */
    public function setMainApp($mainApp)
    {
        $this->mainApp = $mainApp;
    }



    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return Setting
     */
    public function getBusinessGroup()
    {
        return $this->businessGroup;
    }

    /**
     * @param Setting $businessGroup
     */
    public function setBusinessGroup($businessGroup)
    {
        $this->businessGroup = $businessGroup;
    }

    /**
     * @return string
     */
    public function getUniqueKey()
    {
        return $this->uniqueKey;
    }

    /**
     * @param string $uniqueKey
     */
    public function setUniqueKey($uniqueKey)
    {
        $this->uniqueKey = $uniqueKey;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return AppBundle
     */
    public function getAppBundles()
    {
        return $this->appBundles;
    }

    /**
     * @param AppBundle $appBundles
     */
    public function setAppBundles($appBundles)
    {
        $this->appBundles = $appBundles;
    }

    public function getBundleIds()
    {

        $array = array();

        /* @var $bundle AppBundle */

        foreach ($this->getAppBundles() as $bundle){
            $array[] =  $bundle->getId();
        }
        return $array;

    }

    /**
     * @return Domain
     */
    public function getCoreDomain()
    {
        return $this->coreDomain;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getTinNo()
    {
        return $this->tinNo;
    }

    /**
     * @param string $tinNo
     */
    public function setTinNo($tinNo)
    {
        $this->tinNo = $tinNo;
    }

    /**
     * @return string
     */
    public function getBinNo()
    {
        return $this->binNo;
    }

    /**
     * @param string $binNo
     */

    public function setBinNo($binNo)
    {
        $this->binNo = $binNo;
    }

    /**
     * @return bool
     */
    public function isApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param bool $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }







}
