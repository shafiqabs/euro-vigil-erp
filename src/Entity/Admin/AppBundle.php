<?php

namespace App\Entity\Admin;

use App\Entity\Domain\BundleRoleGroup;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * AppModule
 *
 * @ORM\Table("tbd_bundle")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\AppBundleRepository")
 */
class AppBundle
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Terminal", mappedBy="mainApp")
     **/
    private $appDomains;


    /**
     * @var BundleRoleGroup
     * @ORM\OneToMany(targetEntity="App\Entity\Domain\BundleRoleGroup", mappedBy="appBundle")
     **/
    private $bundleRoleGroups;


    /**
     * @ORM\ManyToMany(targetEntity="Terminal", mappedBy="appBundles")
     **/
    private $terminals;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;

    /**
     * @Assert\File(maxSize="8388608")
     */
    protected $file;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="short_name", type="string", length=100,nullable=true)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="app_url", type="string", length=100,nullable=true)
     */
    private $appUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $appBackground = "dark-bg";

    /**
     * @var string
     *
     * @ORM\Column(name="module_class", type="string", length=255, nullable=true)
     */
    private $moduleClass;


    /**
     * @Gedmo\Slug(fields={"moduleClass"})
     * @ORM\Column(length=255, unique=true , nullable = true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text" , nullable = true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="application_manual", type="text" , nullable = true)
     */
    private $applicationManual;

    /**
     * @var string
     *
     * @ORM\Column(name="short_content", type="text" , nullable = true)
     */
    private $shortContent;


    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float" , nullable = true)
     */
    private $price;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AppModule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return AppModule
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return AppModule
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }


    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Sets file.
     *
     * @param AppModule $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return AppModule
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/' . $this->path;
    }



    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/admin/content';
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename ;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }



    /**
     * @return string
     */
    public function getShortContent(){
        return $this->shortContent;
    }

    /**
     * @param string $shortContent
     */
    public function setShortContent( string $shortContent ) {
        $this->shortContent = $shortContent;
    }

    /**
     * @return Terminal
     */
    public function getAppDomains()
    {
        return $this->appDomains;
    }

    /**
     * @return string
     */
    public function getApplicationManual()
    {
        return $this->applicationManual;
    }

    /**
     * @param string $applicationManual
     */
    public function setApplicationManual($applicationManual)
    {
        $this->applicationManual = $applicationManual;
    }

    /**
     * @return Terminal
     */
    public function getTerminals()
    {
        return $this->terminals;
    }

    /**
     * @return string
     */
    public function getModuleClass()
    {
        return $this->moduleClass;
    }

    /**
     * @param string $moduleClass
     */
    public function setModuleClass($moduleClass)
    {
        $this->moduleClass = $moduleClass;
    }

    /**
     * @return string
     */
    public function getShortName(): ? string
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     */
    public function setShortName(string $shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @return string
     */
    public function getAppUrl()
    {
        return $this->appUrl;
    }

    /**
     * @param string $appUrl
     */
    public function setAppUrl($appUrl)
    {
        $this->appUrl = $appUrl;
    }

    /**
     * @return string
     */
    public function getAppBackground(): ? string
    {
        return $this->appBackground;
    }

    /**
     * @param string $appBackground
     */
    public function setAppBackground(string $appBackground)
    {
        $this->appBackground = $appBackground;
    }

    /**
     * @return BundleRoleGroup
     */
    public function getBundleRoleGroups()
    {
        return $this->bundleRoleGroups;
    }

}

